#include "exportToGnuplot.hpp"

bool exportToGnuplot3d (const char* filename, std::vector<double>& xValues,
    std::vector<double>& yValues, std::vector<std::vector<double> >& zValues)
{
    std::ofstream outputFile;
    outputFile.open(filename, std::ofstream::out | std::ofstream::trunc);
    if(!outputFile.is_open())
        return false;

    for (unsigned int i=0; i<xValues.size(); ++i)
    {
        for (unsigned int j=0; j<yValues.size(); ++j)
        {
            outputFile << xValues[i] << " " << yValues[j] << " " << zValues[i][j] << std::endl;
        }
        outputFile << std::endl;
    }
    
    outputFile.close();
    return true;
}

bool exportToGnuplot2d (const char* filename, std::vector<double>& xValues,
    std::vector<double>& yValues)
{
    std::ofstream outputFile;
    outputFile.open(filename, std::ofstream::out | std::ofstream::trunc);
    if(!outputFile.is_open())
        return false;

    for (unsigned int i=0; i<xValues.size(); ++i)
    {
        outputFile << xValues[i] << " " << yValues[i] << std::endl;
    }
    
    outputFile.close();
    return true;
}
