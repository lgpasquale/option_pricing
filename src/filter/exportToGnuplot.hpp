#ifndef _EXPORTTOGNUPLOT_HPP
#define _EXPORTTOGNUPLOT_HPP
#include <fstream>
#include <vector>

bool exportToGnuplot3d (const char* filename, std::vector<double>& xValues,
    std::vector<double>& yValues, std::vector<std::vector<double> >& zValues);

bool exportToGnuplot2d (const char* filename, std::vector<double>& xValues,
    std::vector<double>& yValues);

#endif
