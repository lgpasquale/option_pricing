#ifndef _FEAMERICANCALL_HPP
#define _FEAMERICANCALL_HPP

#include <omp.h>
#include "FEOption.hpp"

class FEAmericanPut: public FEOption
{
public:
    FEAmericanPut ();
    FEAmericanPut (double lowAssetPrice, double highAssetPrice, LifeV::UInt numElements);
    
    void price (double strike, double maturity, double riskFreeRate, double dividendYield, lvPtr_Type localVolatility, int numTimeSteps);
    
protected:
};

#endif
