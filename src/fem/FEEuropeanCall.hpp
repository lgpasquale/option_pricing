#ifndef _FEEUROPEANCALL_HPP
#define _FEEUROPEANCALL_HPP

#include "FEOption.hpp"

class FEEuropeanCall: public FEOption
{
public:
    FEEuropeanCall ();
    FEEuropeanCall (double lowAssetPrice, double highAssetPrice, LifeV::UInt numElements);
    
    void price (double strike, double maturity, double riskFreeRate, double dividendYield, lvPtr_Type localVolatility, int numTimeSteps);
    
protected:

};

#endif
