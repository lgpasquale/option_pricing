#ifndef _FEASIANARITHMETICAVERAGESTRIKECALL_HPP
#define _FEASIANARITHMETICAVERAGESTRIKECALL_HPP

#include "FEOption.hpp"

class FEAsianArithmeticAverageStrikeCall: public FEOption
{
public:
    FEAsianArithmeticAverageStrikeCall ();
    FEAsianArithmeticAverageStrikeCall (double lowAssetPrice, double highAssetPrice, LifeV::UInt numElements);
    
    bool exportSolutionToGnuplot(const char* gnuplotFilename, const int numSteps);
    
    void price (double maturity, double riskFreeRate, double dividendYield, lvPtr_Type localVolatility, int numTimeSteps);
    double getPrice(const double assetPrice);
    double getPrice(const double assetPrice, const double average);
    
protected:
    std::vector<vectorPtr_Type> M_solutionsS;
    std::vector<vectorPtr_Type> M_solutionsA;

    boost::shared_ptr< mesh_Type > M_aMeshPtr;
    feSpacePtr_Type M_aFESpace;
    etfeSpacePtr_Type M_aETFESpace;
};

#endif
