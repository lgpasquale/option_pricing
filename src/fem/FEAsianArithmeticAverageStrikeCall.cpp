#include "FEAsianArithmeticAverageStrikeCall.hpp"


using namespace LifeV;

class PayoffAsianArithmeticAverageStrikeCall
{
public:
    PayoffAsianArithmeticAverageStrikeCall (const double& maturity) : M_T(maturity) {}
    void setAssetPrice(const double& assetPrice) {M_assetPrice=assetPrice;}
    Real operator() ( const Real& /* t */, const Real& x, const Real& /* y */,
                          const Real& /* z */, const ID&   /* ic */)
    {
        return std::max( M_assetPrice-x/M_T , 0.0);
    }
protected:
    double M_assetPrice;
    double M_T;
};

class AsianArithmeticAverageStrikeDiffusionCoeff
{
public:
    AsianArithmeticAverageStrikeDiffusionCoeff (FEOption::lvPtr_Type localVolatility) : M_lv(localVolatility) {}
    LifeV::Real operator() ( const LifeV::Real& t, const LifeV::Real& x, const LifeV::Real& /* y */,
                          const LifeV::Real& /* z */, const LifeV::ID&   /* ic */)
    {
        double lv=M_lv->getLocalVolatility(x,t);
        return 0.5*lv*lv*x*x;
    }
protected:
    FEOption::lvPtr_Type M_lv;
};

FEAsianArithmeticAverageStrikeCall::FEAsianArithmeticAverageStrikeCall (double lowAssetPrice, double highAssetPrice, LifeV::UInt numElements):
    FEOption(lowAssetPrice, highAssetPrice, numElements),
    M_solutionsA(M_uFESpace->dof().numTotalDof())
{

}

void FEAsianArithmeticAverageStrikeCall::price (double maturity, double riskFreeRate, double dividendYield, lvPtr_Type localVolatility, int numTimeSteps)
{
    M_T=maturity;
    M_r=riskFreeRate;
    M_q=dividendYield;
    M_lv=localVolatility;
    double dt=maturity/static_cast<double> (numTimeSteps);
    
    M_aMeshPtr.reset ( new mesh_Type(M_comm) );

    regularMesh1D ( *M_aMeshPtr, 0, M_numElements, true,
                    M_highAssetPrice*maturity,
                    M_lowAssetPrice);

    std::string uOrder ("P1");

    M_aFESpace.reset( new feSpace_Type (M_aMeshPtr, feSegP1, quadRuleSeg1pt, quadRuleNode1pt, 1, M_comm) );
    M_aETFESpace.reset( new etfeSpace_Type (M_aMeshPtr, & (M_aFESpace->refFE() ), & (M_aFESpace->fe().geoMap() ), M_comm) );

    M_solutionsS.resize(M_aFESpace->dof().numTotalDof());

    for (unsigned int i=0; i<M_solutionsS.size(); ++i)
    {
        M_solutionsS[i].reset( new vector_Type(M_uFESpace->map(), Unique) );
    }
    for (unsigned int i=0; i<M_solutionsA.size(); ++i)
    {
        M_solutionsA[i].reset( new vector_Type(M_aFESpace->map(), Unique) );
    }
    
    // Create the functors for B.C. and I.C.
    PayoffAsianArithmeticAverageStrikeCall payoff(maturity);
    
    AsianArithmeticAverageStrikeDiffusionCoeff diffCoeff(M_lv);

    vector_Type assetPriceInterpolated (M_uFESpace->map(), Repeated);
    M_uFESpace->interpolate ( static_cast<feSpace_Type::function_Type> (assetPriceFct), assetPriceInterpolated, 0.0 );

    VectorSmall<1> oneVector;
    oneVector[0]=1.0;
    
    // Create the vector containing the solution obtained
    // at the previous step and initialize it to (S-K)+
    vectorPtr_Type solutionOldS ( new vector_Type(M_uFESpace->map(), Repeated) );
    vectorPtr_Type solutionOldA ( new vector_Type(M_aFESpace->map(), Repeated) );
    solutionOldS->zero();
    solutionOldA->zero();
    for (unsigned int j=0; j<M_solutionsA.size(); ++j)
    {
        payoff.setAssetPrice(assetPriceInterpolated[j]);
        M_aFESpace->interpolate ( static_cast<feSpace_Type::function_Type> (payoff), *M_solutionsA[j], 0.0 );
    }
    
    for (int i=numTimeSteps-1; i>=0; --i)
    {
        for (unsigned int j=0; j<M_solutionsS.size(); ++j)
        {
            for (int k=0; k<M_solutionsS[j]->size(); ++k)
            {
                (*solutionOldS)[k]=(*M_solutionsA[k])[j];
            }

            vector_Type diffCoeffInterpolated (M_uFESpace->map(), Repeated);
            M_uFESpace->interpolate ( static_cast<feSpace_Type::function_Type> (diffCoeff), diffCoeffInterpolated, i*dt );

            // Build the matrix and vectors
            boost::shared_ptr<matrix_Type> systemMatrix (new matrix_Type ( M_uFESpace->map() ) );
            systemMatrix->zero();
            vector_Type rhs (M_uFESpace->map(), Repeated);
            rhs.zero();
            
            {
                using namespace ExpressionAssembly;
        
                integrate ( elements (M_uETFESpace->mesh() ),
                            M_uFESpace->qr(),
                            M_uETFESpace,
                            M_uETFESpace,
        
                            -1.0 * value(M_uETFESpace, diffCoeffInterpolated)*dot ( grad (phi_i) , grad (phi_j) )
                            - dot ( grad (phi_j), grad(M_uETFESpace, diffCoeffInterpolated) ) * phi_i
                            + dot ( grad (phi_j) , value(oneVector) ) * value (M_uETFESpace, assetPriceInterpolated) * value(M_r-M_q) *phi_i
                            - value(M_r)*phi_j*phi_i
                            - phi_i * phi_j / value(dt)
        
                          )
                        >> systemMatrix;
        
                integrate ( elements (M_uETFESpace->mesh() ),
                            M_uFESpace->qr(),
                            M_uETFESpace,
        
                            value (M_uETFESpace, *solutionOldS) * phi_i / value(-dt) 
        
                          )
                        >> rhs;
            }
    
            systemMatrix->globalAssemble();
            rhs.globalAssemble();
    
            BCHandler bchandler;
    
            BCFunctionBase leftBC ( static_cast<feSpace_Type::function_Type> ( zeroFct ) );
            diffCoeffInterpolated*=-1;
            BCVector rightBC(diffCoeffInterpolated,M_uFESpace->dof().numTotalDof(),0);
    
            bchandler.addBC ("Dirichlet", Structured1DLabel::LEFT, Essential, Full, leftBC, 1);
            bchandler.addBC ("Neumann", Structured1DLabel::RIGHT, Natural, Full, rightBC, 1);
    
            bchandler.bcUpdate (*M_uFESpace->mesh(), M_uFESpace->feBd(), M_uFESpace->dof() );
            vectorPtr_Type rhsBC( new vector_Type (rhs, Unique) );
            bcManage (*systemMatrix, *rhsBC, *M_uFESpace->mesh(), M_uFESpace->dof(), bchandler, M_uFESpace->feBd(), 1.0, i*dt);
            rhs = *rhsBC;
    
            // Solve the linear problem
            std::cout << "Solving linear system for parabolic problem at iteration " << i << "for t=" << i*dt << std::endl;
            M_linearSolver.setOperator (systemMatrix);
            M_linearSolver.setRightHandSide (rhsBC);
            M_linearSolver.solve (M_solutionsS[j]);
            
        }
       
        for (unsigned int j=0; j<M_solutionsA.size(); ++j)
        {
            for (int k=0; k<M_solutionsA[j]->size(); ++k)
            {
                (*solutionOldA)[k]=(*M_solutionsS[k])[j];
            }
            
            // Build the matrix and vectors
            boost::shared_ptr<matrix_Type> systemMatrix (new matrix_Type ( M_aFESpace->map() ) );
            systemMatrix->zero();
            vector_Type rhs (M_aFESpace->map(), Repeated);
            rhs.zero();
            //double delta=1.0;
            
            {
                using namespace ExpressionAssembly;
        
                integrate ( elements (M_aETFESpace->mesh() ),
                            //M_aFESpace->qr(),
                            quadRuleSeg3pt,
                            M_aETFESpace,
                            M_aETFESpace,
        
                            -1.0* phi_i * phi_j / value(dt)

                          )
                        >> systemMatrix;
        
                integrate ( elements (M_aETFESpace->mesh() ),
                            quadRuleSeg3pt,
                            M_aETFESpace,
        
                            value (M_aETFESpace, *solutionOldA) * phi_i / value(-dt) 
                            - dot ( grad (M_aETFESpace, *solutionOldA) , value(oneVector) ) * value (assetPriceInterpolated[j]) *phi_i
                            + value(assetPriceInterpolated[j]*assetPriceInterpolated[j]/2.*dt) *dot ( grad (phi_i) , grad (M_aETFESpace, *solutionOldA) )
        
                          )
                        >> rhs;
            }
    
            systemMatrix->globalAssemble();
            rhs.globalAssemble();
    
            BCHandler bchandler;
    
            vector_Type leftBCNeumannVector(M_aFESpace->map(),Repeated);
            leftBCNeumannVector = M_aFESpace->gradientRecovery(*solutionOldA,0);
            //leftBCNeumannVector = ((*solutionOldA)[1]-(*solutionOldA)[0])/std::abs(M_aFESpace->mesh()->boundaryPoint(1).x()-M_aFESpace->mesh()->boundaryPoint(0).x())*M_aFESpace->mesh()->numElements();
            leftBCNeumannVector*=(assetPriceInterpolated[j]*assetPriceInterpolated[j]/2.*dt);
            //leftBCNeumannVector*=(hyperbolicAdvCoeffInterpolated[0]*hyperbolicAdvCoeffInterpolated[0]/2.*dt);
            //leftBCNeumannVector*=(assetPriceInterpolated[j]*(10-0.1)/50/2.);
            //leftBCNeumannVector+=(*solutionOld*assetPriceInterpolated[j]);
            BCVector leftBC(leftBCNeumannVector,M_uFESpace->dof().numTotalDof(),0);
            
            //BCFunctionRobin leftBC ( static_cast<feSpace_Type::function_Type> ( robinRhsBCFct ), static_cast<feSpace_Type::function_Type> ( robinMassCoeffBCFct ) );
            BCFunctionBase rightBC ( static_cast<feSpace_Type::function_Type> ( zeroFct ) );
    
            bchandler.addBC ("Neumann", Structured1DLabel::LEFT, Natural, Full, leftBC, 1);
            bchandler.addBC ("Dirichlet", Structured1DLabel::RIGHT, Essential, Full, rightBC, 1);
    
            bchandler.bcUpdate (*M_aFESpace->mesh(), M_aFESpace->feBd(), M_aFESpace->dof() );
            vectorPtr_Type rhsBC( new vector_Type (rhs, Unique) );
            bcManage (*systemMatrix, *rhsBC, *M_uFESpace->mesh(), M_aFESpace->dof(), bchandler, M_aFESpace->feBd(), 1.0, i*dt);
            rhs = *rhsBC;
    
            // Solve the linear problem
            std::cout << "Solving linear system for hyperbolic problem aat iteration " << i << "for t=" << i*dt << std::endl;
            M_linearSolver.setOperator (systemMatrix);
            M_linearSolver.setRightHandSide (rhsBC);
            M_linearSolver.solve (M_solutionsA[j]);
            
            // Finite Difference Upwind
            //(*M_solutionsA[j])[M_solutionsA[j]->size()-1]=0.0;
            //for (int k=0; k<M_solutionsA[j]->size()-1; ++k)
            //{
                //(*M_solutionsA[j])[k]=(*M_solutionsS[k])[j]+dt/(10-0.1)*50*( (*M_solutionsS[k+1])[j] - (*M_solutionsS[k])[j] );
            //}
        }
            
    }
}

double FEAsianArithmeticAverageStrikeCall::getPrice(const double assetPrice)
{
    return getPrice(assetPrice, 0.0);
}

double FEAsianArithmeticAverageStrikeCall::getPrice(const double assetPrice, const double average)
{
    double firstS;
    double secondS;
    LifeV::UInt firstID;
    LifeV::UInt secondID;

    for (unsigned int i=0; i<M_uFESpace->mesh()->numElements(); ++i)
    {
        if ( std::min( M_uFESpace->mesh()->element(i).point(0).coordinate(0),
            M_uFESpace->mesh()->element(i).point(1).coordinate(0) ) <= assetPrice &&
            std::max( M_uFESpace->mesh()->element(i).point(0).coordinate(0),
            M_uFESpace->mesh()->element(i).point(1).coordinate(0) )>= assetPrice )
        {
            firstS = M_uFESpace->mesh()->element(i).point(0).coordinate(0);
            secondS = M_uFESpace->mesh()->element(i).point(1).coordinate(0);
            firstID = M_uFESpace->dof().localToGlobalMap(i,0);
            secondID = M_uFESpace->dof().localToGlobalMap(i,1);
            break;
        }
    }
    for (unsigned int i=0; i<M_aFESpace->mesh()->numElements(); ++i)
    {
        if ( std::min( M_aFESpace->mesh()->element(i).point(0).coordinate(0),
            M_aFESpace->mesh()->element(i).point(1).coordinate(0) ) <= average &&
            std::max( M_aFESpace->mesh()->element(i).point(0).coordinate(0),
            M_aFESpace->mesh()->element(i).point(1).coordinate(0) )>= average )
        {
            std::vector<LifeV::Real> point (1,average);
            double firstValue = M_aFESpace->feInterpolateValue(i, *M_solutionsA[firstID], point, 0);
            double secondValue = M_aFESpace->feInterpolateValue(i, *M_solutionsA[secondID], point, 0);
            return (assetPrice-firstS)*firstValue/(secondS-firstS)
                + (assetPrice-secondS)*secondValue/(firstS-secondS);
        }
    }
    std::cout << "Failed to find point " << assetPrice << " " << average << std::endl;
    return 0;
}

bool FEAsianArithmeticAverageStrikeCall::exportSolutionToGnuplot(const char* gnuplotFilename, const int numSteps)
{
    double xl = std::min(M_uFESpace->mesh()->boundaryPoint(0).x(),M_uFESpace->mesh()->boundaryPoint(1).x());
    double xr = std::max(M_uFESpace->mesh()->boundaryPoint(0).x(),M_uFESpace->mesh()->boundaryPoint(1).x());
    double dx=(xr-xl)/numSteps;
    double yl = std::min(M_aFESpace->mesh()->boundaryPoint(0).x(),M_aFESpace->mesh()->boundaryPoint(1).x());
    double yr = std::max(M_aFESpace->mesh()->boundaryPoint(0).x(),M_aFESpace->mesh()->boundaryPoint(1).x());
    double dy=(yr-yl)/numSteps;

    std::vector<double> xValues(numSteps+1);
    std::vector<double> yValues(numSteps+1);
    std::vector<std::vector<double> > zValues(numSteps+1, std::vector<double>(numSteps+1) );
    for (int i=0; i<=numSteps; ++i)
    {
        xValues[i]=xl+dx*i;
        yValues[i]=yl+dy*i;
    }
    for (int i=0; i<=numSteps; ++i)
    {
        for (int j=0; j<=numSteps; ++j)
        {
            zValues[i][j]=getPrice(xValues[i],yValues[j]);
        }
    }
    return exportToGnuplot3d(gnuplotFilename, xValues, yValues, zValues);
}
