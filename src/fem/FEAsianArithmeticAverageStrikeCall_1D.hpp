#ifndef _FEASIANARITHMETICAVERAGESTRIKECALL_HPP
#define _FEASIANARITHMETICAVERAGESTRIKECALL_HPP

#include "FEOption.hpp"

class FEAsianArithmeticAverageStrikeCall: public FEOption
{
public:
    FEAsianArithmeticAverageStrikeCall ();
    FEAsianArithmeticAverageStrikeCall (double lowAssetPrice, double highAssetPrice, LifeV::UInt numElements);
    
    void price (double strike, double maturity, double riskFreeRate, double dividendYield, lvPtr_Type localVolatility, int numTimeSteps);
    
protected:
    std::vector<vectorPtr_Type> M_solutions;
};

#endif
