#include "FEOption.hpp"

using namespace LifeV;


FEOption::FEOption (double lowAssetPrice, double highAssetPrice, LifeV::UInt numElements):
    M_lowAssetPrice(lowAssetPrice), M_highAssetPrice(highAssetPrice), M_numElements(numElements)
{
#ifdef HAVE_MPI
    //M_comm.reset ( new Epetra_MpiComm ( MPI_COMM_WORLD ) );
    M_comm.reset ( new Epetra_MpiComm(MPI_COMM_SELF) );
#else
    M_comm.reset ( new Epetra_SerialComm() );
#endif

    M_verbose = (M_comm->MyPID() == 0);

    M_meshPtr.reset ( new mesh_Type(M_comm) );

    regularMesh1D ( *M_meshPtr, 0, M_numElements, true,
                    M_highAssetPrice,
                    M_lowAssetPrice);

    //MeshPartitioner< mesh_Type >   meshPart (fullMeshPtr, M_comm);
    //boost::shared_ptr< mesh_Type > meshPtr (meshPart.meshPartition() );

    //fullMeshPtr.reset();

    if (M_verbose)
    {
        std::cout << " -- Building FESpaces ... " << std::flush;
    }

    std::string uOrder ("P1");

    //M_uFESpace.reset( new FESpace< mesh_Type, MapEpetra > (fullMeshPtr, uOrder, 1, M_comm) );
    M_uFESpace.reset( new feSpace_Type (M_meshPtr, feSegP1, quadRuleSeg2pt, quadRuleNode1pt, 1, M_comm) );

    if (M_verbose)
    {
        std::cout << " done ! " << std::endl;
        std::cout << " ---> Dofs: " << M_uFESpace->dof().numTotalDof() << std::endl;
        std::cout << " -- Building ETFESpaces ... " << std::flush;
    }

    M_uETFESpace.reset( new etfeSpace_Type (M_meshPtr, & (M_uFESpace->refFE() ), & (M_uFESpace->fe().geoMap() ), M_comm) );

    if (M_verbose)
    {
        std::cout << " done ! " << std::endl;
        std::cout << " ---> Dofs: " << M_uETFESpace->dof().numTotalDof() << std::endl;
    }

    createLinearSolver();
}

void FEOption::exportSolution(const char* hdf5Filename)
{
#ifdef HAVE_HDF5
    // Set the exporter for the solution
    boost::shared_ptr< Exporter< mesh_Type > > exporter (new ExporterHDF5< mesh_Type > () );

    // Set output file name
    exporter->setPrefix(std::string(hdf5Filename));

    exporter->setMeshProcId ( M_meshPtr, M_comm->MyPID() );

    // Export the partitioning
    exporter->exportPID ( M_meshPtr, M_comm );

    // Shared pointer used in the exporter for the solution
    vectorPtr_Type exporterSolution;

    // Set the exporter solution
    exporterSolution.reset ( new vector_Type ( *M_solution,
                                               exporter->mapType() ) );
    
    if (exporter->mapType() == Unique)
        std::cout << "Exporter type: unique" << std::endl;
    else
        std::cout << "Exporter type: repeated" << std::endl;

    // Add the solution to the exporter
    exporter->addVariable ( ExporterData<mesh_Type>::ScalarField,
                            "OptionPrice", M_uFESpace,
                            exporterSolution,
                            static_cast<UInt> ( 0 ),
                            ExporterData<mesh_Type>::SteadyRegime,
                            ExporterData<mesh_Type>::Node );

    *exporterSolution = *M_solution;
    exporter->postProcess (0.0);

#else
    if (M_verbose)
    {
        std::cout << "HDF5 exporter not avilable" << std::endl;
    }
#endif

}

void FEOption::createLinearSolver()
{
    Teuchos::RCP< Teuchos::ParameterList > aztecooList = Teuchos::rcp ( new Teuchos::ParameterList );
    aztecooList = Teuchos::getParametersFromXmlFile ( "SolverParamList.xml" );
    Teuchos::RCP< Teuchos::ParameterList > mlList = Teuchos::rcp ( new Teuchos::ParameterList );
    mlList = Teuchos::getParametersFromXmlFile ( "PrecParamList.xml" );

    M_linearSolver.setCommunicator (M_comm);
    M_linearSolver.setParameters (*aztecooList);

    PreconditionerML* precRawPtr;
    boost::shared_ptr<Preconditioner> precPtr;
    precRawPtr = new PreconditionerML;
    precRawPtr->setParametersList (*mlList );
    precPtr.reset ( precRawPtr );
    M_linearSolver.setPreconditioner ( precPtr );

}

double FEOption::getPrice(const double assetPrice)
{
    for (unsigned int i=0; i<M_uFESpace->mesh()->numElements(); ++i)
    {
        if ( std::min( M_uFESpace->mesh()->element(i).point(0).coordinate(0),
            M_uFESpace->mesh()->element(i).point(1).coordinate(0) ) <=assetPrice &&
            std::max( M_uFESpace->mesh()->element(i).point(0).coordinate(0),
            M_uFESpace->mesh()->element(i).point(1).coordinate(0) )>=assetPrice )
        {
            std::vector<LifeV::Real> point (1,assetPrice);
            return M_uFESpace->feInterpolateValue(i, *M_solution, point, 0);
        }
    }
    return 0;
}

bool FEOption::exportSolutionToGnuplot(const char* gnuplotFilename, const int numSteps)
{
    double xl = std::min(M_uFESpace->mesh()->boundaryPoint(0).x(),M_uFESpace->mesh()->boundaryPoint(1).x());
    double xr = std::max(M_uFESpace->mesh()->boundaryPoint(0).x(),M_uFESpace->mesh()->boundaryPoint(1).x());
    double dx=(xr-xl)/numSteps;

    std::ofstream outputFile;
    outputFile.open(gnuplotFilename, std::ofstream::out | std::ofstream::trunc);
    if(!outputFile.is_open())
        return false;

    for (int i=0; i<=numSteps; ++i)
    {
        outputFile << xl+dx*i << " " << getPrice(xl+dx*i) << std::endl;
    }
    
    outputFile.close();
    return true;
}


LifeV::Real assetPriceFct ( const LifeV::Real& /* t */, const LifeV::Real& x, const LifeV::Real& /*y*/, const LifeV::Real& /* z */, const LifeV::ID& /* i */ )
{
    return x;
}

Real zeroFct ( const Real& /* t */, const Real& /* x */, const Real& /*y*/, const Real& /* z */, const ID& /* i */ )
{
    return 0.0;
}

Real oneFct ( const Real& /* t */, const Real& /* x */, const Real& /*y*/, const Real& /* z */, const ID& /* i */ )
{
    return 1.0;
}
