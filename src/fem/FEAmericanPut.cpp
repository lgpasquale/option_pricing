#include "FEAmericanPut.hpp"

using namespace LifeV;

class PayoffAmericanPut
{
public:
    PayoffAmericanPut (double strike) : M_K(strike) {}
    Real operator() ( const Real& /* t */, const Real& x, const Real& /* y */,
                          const Real& /* z */, const ID&   /* ic */)
    {
        return std::max( M_K-x , 0.0);
    }
protected:
    double M_K;
};

class AmericanPutLeftBCFct
{
public:
    AmericanPutLeftBCFct (double strike) : M_K(strike) {}
    Real operator() ( const Real& /* t */, const Real& x, const Real& /* y */,
                          const Real& /* z */, const ID&   /* ic */)
    {
        double beta = 0.5*(x);
        return std::max( M_K-std::exp(x) , 0.0)*std::exp(-beta);
    }
protected:
    double M_K;
};

class AmericanPutDiffusionCoeff
{
public:
    AmericanPutDiffusionCoeff (FEOption::lvPtr_Type localVolatility) : M_lv(localVolatility) {}
    LifeV::Real operator() ( const LifeV::Real& t, const LifeV::Real& x, const LifeV::Real& /* y */,
                          const LifeV::Real& /* z */, const LifeV::ID&   /* ic */)
    {
        double lv=M_lv->getLocalVolatility(x,t);
        return 0.5*lv*lv*x*x;
    }
protected:
    FEOption::lvPtr_Type M_lv;
};

FEAmericanPut::FEAmericanPut (double lowAssetPrice, double highAssetPrice, LifeV::UInt numElements):
    FEOption(lowAssetPrice, highAssetPrice, numElements)
{

}

void FEAmericanPut::price (double strike, double maturity, double riskFreeRate, double dividendYield, lvPtr_Type localVolatility, int numTimeSteps)
{
    M_K=strike;
    M_T=maturity;
    M_r=riskFreeRate;
    M_q=dividendYield;
    M_lv=localVolatility;
    double dt=maturity/static_cast<double> (numTimeSteps);
    
    M_solution.reset( new vector_Type(M_uFESpace->map(), Unique) );
    
    // Create the functors for B.C. and I.C.
    PayoffAmericanPut payoff(M_K);
    
    vector_Type payoffInterpolated (M_uFESpace->map(), Repeated);
    M_uFESpace->interpolate ( static_cast<feSpace_Type::function_Type> (payoff), payoffInterpolated, 0.0 );
        
    AmericanPutDiffusionCoeff diffCoeff(M_lv);
    
    VectorSmall<1> oneVector;
    oneVector[0]=1.0;
    
    // Create the vector containing the solution obtained
    // at the previous step and initialize it to (S-K)+
    vector_Type solutionOld (M_uFESpace->map(), Unique);
    solutionOld *= 0.0;
    M_uFESpace->interpolate ( static_cast<feSpace_Type::function_Type> (payoff), solutionOld, 0.0 );
    
    for (int i=numTimeSteps-1; i>=0; --i)
    {std::cout<<i*dt<<std::endl;
        vector_Type assetPriceInterpolated (M_uFESpace->map(), Repeated);
        M_uFESpace->interpolate ( static_cast<feSpace_Type::function_Type> (assetPriceFct), assetPriceInterpolated, double(i)*dt );

        vector_Type diffCoeffInterpolated (M_uFESpace->map(), Repeated);
        M_uFESpace->interpolate ( static_cast<feSpace_Type::function_Type> (diffCoeff), diffCoeffInterpolated, double(i)*dt );

        // Build the matrix and vectors
        boost::shared_ptr<matrix_Type> systemMatrix (new matrix_Type ( M_uFESpace->map() ) );
        *systemMatrix *= 0.0;
        vector_Type rhs (M_uFESpace->map(), Repeated);
        rhs *= 0.0;
        
        {
            using namespace ExpressionAssembly;
    
            integrate ( elements (M_uETFESpace->mesh() ),
                        M_uFESpace->qr(),
                        M_uETFESpace,
                        M_uETFESpace,
    
                        value(M_uETFESpace, diffCoeffInterpolated)*dot ( grad (phi_i) , grad (phi_j) )
                        + dot ( grad (phi_j), grad(M_uETFESpace, diffCoeffInterpolated) ) * phi_i
                        - dot ( grad (phi_j) , value(oneVector) ) * value (M_uETFESpace, assetPriceInterpolated) * value(M_r-M_q)  *phi_i
                        + value(M_r) * phi_i * phi_j
                        + phi_i * phi_j / value(dt)
    
                      )
                    >> systemMatrix;
    
            integrate ( elements (M_uETFESpace->mesh() ),
                        M_uFESpace->qr(),
                        M_uETFESpace,
    
                        value (M_uETFESpace, vector_Type(solutionOld,Repeated)) * phi_i / value(dt) 
   
                      )
                    >> rhs;
        }

        systemMatrix->globalAssemble();
        rhs.globalAssemble();

        BCHandler bchandler;

        BCFunctionBase leftBC ( static_cast<feSpace_Type::function_Type> ( payoff ) );
        BCFunctionBase rightBC ( static_cast<feSpace_Type::function_Type> ( zeroFct ) );

        bchandler.addBC ("Dirichlet", Structured1DLabel::LEFT, Essential, Full, leftBC, 1);
        bchandler.addBC ("Dirichlet", Structured1DLabel::RIGHT, Essential, Full, rightBC, 1);

        bchandler.bcUpdate (*M_uFESpace->mesh(), M_uFESpace->feBd(), M_uFESpace->dof() );
        vectorPtr_Type rhsBC( new vector_Type (rhs, Unique) );
        bcManage (*systemMatrix, *rhsBC, *M_uFESpace->mesh(), M_uFESpace->dof(), bchandler, M_uFESpace->feBd(), 1.0, static_cast<double> (i)*dt);
        rhs = *rhsBC;
        systemMatrix->globalAssemble();
        
        vector_Type y(M_uFESpace->map(), Unique);
        vector_Type psorSolution(solutionOld, Unique);
        vector_Type psorSolutionOld(solutionOld, Unique);
        vector_Type errorVector(M_uFESpace->map(), Unique);
        double omega=1.0;
        double eps=1.e-8;
        int maxIter=1000;
        double error=1.;
        int iter=0;
        
        //boost::shared_ptr<matrix_Type> transposedMatrix(systemMatrix->transpose());
        //boost::shared_ptr<Epetra_FECrsMatrix> epetraMatrix(transposedMatrix->matrixPtr());
        boost::shared_ptr<Epetra_FECrsMatrix> epetraMatrix(systemMatrix->matrixPtr());
        while (error>eps && iter<maxIter)
        {
            for (int j=0; j<y.size(); ++j)
            {
                y[j]=0;
                int numEntries;
                double* values;
                int* indices;
                if (epetraMatrix->ExtractMyRowView(j, numEntries, values, indices))
                {
                    std::cout << "Failed to extract row from Epetra_FECrsMatrix" << std::endl;
                }
                
                double diagonalElement(0);
                for (int k=0; k<numEntries; ++k)
                {
                    if (indices[k]<j)
                    {
                        y[j]-=values[k]*psorSolution[j];
                    }
                    else if (indices[k]>j)
                    {
                        y[j]-=values[k]*psorSolutionOld[j];
                    }
                    else
                    {
                        diagonalElement=values[k];
                    }
                }
                
                y[j]=(y[j]+(*rhsBC)[j])/diagonalElement;
                psorSolution[j]=std::max( payoffInterpolated[j], psorSolutionOld[j]+omega*(y[j]-psorSolutionOld[j]) );
            }
            errorVector=psorSolution-psorSolutionOld;
            error=errorVector.norm2()/psorSolution.norm2();
            iter++;
            psorSolutionOld=psorSolution;
        }
        std::cout << "Number of PSOR iterations: " << iter << "\terror: " << error << std::endl;
        solutionOld=psorSolution;//*(*M_currentExpBeta);
    }
    *M_solution=solutionOld;
}

