#ifndef _FEOPTION_HPP
#define _FEOPTION_HPP
#include <lifev/core/LifeV.hpp>
#include <lifev/core/array/MatrixEpetra.hpp>
#include <lifev/core/array/VectorEpetra.hpp>

#include <lifev/core/mesh/MeshPartitioner.hpp>
#include <lifev/core/mesh/RegionMesh1DStructured.hpp>
#include <lifev/core/mesh/RegionMesh.hpp>

#include <lifev/core/fem/FESpace.hpp>
#include <lifev/eta/fem/ETFESpace.hpp>
#include <lifev/core/fem/BCManage.hpp>

#include <lifev/eta/expression/Integrate.hpp>

#include <lifev/core/algorithm/LinearSolver.hpp>
#include <lifev/core/algorithm/PreconditionerML.hpp>

#include <Teuchos_ParameterList.hpp>
#include <Teuchos_XMLParameterListHelpers.hpp>
#include <Teuchos_RCP.hpp>


#ifdef HAVE_HDF5
#include <lifev/core/filter/ExporterHDF5.hpp>
#endif

#include <iostream>
#include <cmath>

#include <boost/shared_ptr.hpp>
#include <boost/math/distributions.hpp>

#include "../volatility/LocalVolatility.hpp"

class FEOption
{
public:
    typedef LifeV::RegionMesh<LifeV::LinearLine> mesh_Type;
    typedef LifeV::MatrixEpetra<LifeV::Real> matrix_Type;
    typedef LifeV::VectorEpetra vector_Type;
    typedef boost::shared_ptr<vector_Type> vectorPtr_Type;
    typedef LifeV::FESpace< mesh_Type, LifeV::MapEpetra > feSpace_Type;
    typedef LifeV::ETFESpace< mesh_Type, LifeV::MapEpetra, 1, 1 > etfeSpace_Type;
    typedef boost::shared_ptr<feSpace_Type> feSpacePtr_Type;
    typedef boost::shared_ptr<etfeSpace_Type> etfeSpacePtr_Type;
    typedef boost::shared_ptr<LocalVolatility> lvPtr_Type;



    FEOption ();
    FEOption (double lowAssetPrice, double highAssetPrice, LifeV::UInt numElements);
    
    void exportSolution(const char* hdf5Filename);
    virtual bool exportSolutionToGnuplot(const char* gnuplotFilename, const int numSteps);
    
    void setT(const double T) {M_T=T;}
    void setK(const double K) {M_K=K;}
    //void setSigma(double sigma) {M_sigma=sigma;}
    void setLocalVolatility(lvPtr_Type lv) {M_lv=lv;}
    void setR(const double r) {M_r=r;}
    void setQ(const double q) {M_q=q;}
    
    virtual double getPrice(const double assetPrice);
    
protected:
    void createLinearSolver();

    boost::shared_ptr<Epetra_Comm>   M_comm;
    bool M_verbose;

    double M_lowAssetPrice;
    double M_highAssetPrice;
    LifeV::UInt M_numElements;
    double M_T;     // Maturity
    double M_K;     // Strike
    //double M_sigma; // Volatility
    lvPtr_Type M_lv; // Volatility
    double M_r;     // Risk free rate
    double M_q;     // Dividend yield
    
    boost::shared_ptr< mesh_Type > M_meshPtr;
    feSpacePtr_Type M_uFESpace;
    etfeSpacePtr_Type M_uETFESpace;
    
    LifeV::LinearSolver M_linearSolver;
    
    vectorPtr_Type M_solution;

};

class Sigma
{
public:
    Sigma (FEOption::lvPtr_Type localVolatility) : M_lv(localVolatility) {}
    LifeV::Real operator() ( const LifeV::Real& t, const LifeV::Real& x, const LifeV::Real& /* y */,
                          const LifeV::Real& /* z */, const LifeV::ID&   /* ic */)
    {
        return M_lv->getLocalVolatility(x,t);
        //return M_lv->getLocalVolatility(std::exp(x),t);
    }
protected:
    FEOption::lvPtr_Type M_lv;
};


LifeV::Real assetPriceFct ( const LifeV::Real& /* t */, const LifeV::Real& x, const LifeV::Real& /*y*/, const LifeV::Real& /* z */, const LifeV::ID& /* i */ );
LifeV::Real zeroFct ( const LifeV::Real& /* t */, const LifeV::Real& /* x */, const LifeV::Real& /*y*/, const LifeV::Real& /* z */, const LifeV::ID& /* i */ );
LifeV::Real oneFct ( const LifeV::Real& /* t */, const LifeV::Real& /* x */, const LifeV::Real& /*y*/, const LifeV::Real& /* z */, const LifeV::ID& /* i */ );

#endif
