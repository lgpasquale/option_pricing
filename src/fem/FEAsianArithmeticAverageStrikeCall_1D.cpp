#include "FEAsianArithmeticAverageStrikeCall.hpp"


using namespace LifeV;

class PayoffAsianArithmeticAverageStrikeCall
{
public:
    PayoffAsianArithmeticAverageStrikeCall (double maturity) : M_T(maturity) {}
    Real operator() ( const Real& /* t */, const Real& x, const Real& /* y */,
                          const Real& /* z */, const ID&   /* ic */)
    {
        return std::max( 1.0-x/M_T , 0.0);
    }
protected:
    double M_T;
};

class RobinMassCoeffBCFct
{
public:
    RobinMassCoeffBCFct (double dt) : M_dt(dt) {}
    Real operator() ( const Real& /* t */, const Real& /* x */, const Real& /* y */,
                          const Real& /* z */, const ID&   /* ic */)
    {
        return 1.0/M_dt;
    }
protected:
    double M_dt;
};

class RobinRhsBCFct
{
public:
    RobinRhsBCFct (double dt, FEOption::vectorPtr_Type solutionOld) : M_dt(dt), M_solutionOld(solutionOld) {}
    Real operator() ( const Real& /* t */, const Real& /* x */, const Real& /* y */,
                          const Real& /* z */, const ID&   /* ic */)
    {
        return -(*M_solutionOld)[0]/M_dt;
    }
protected:
    double M_dt;
    FEOption::vectorPtr_Type M_solutionOld;
};

class AsianArithmeticAverageStrikeDiffusionCoeff
{
public:
    AsianArithmeticAverageStrikeDiffusionCoeff (FEOption::lvPtr_Type localVolatility) : M_lv(localVolatility) {}
    LifeV::Real operator() ( const LifeV::Real& t, const LifeV::Real& x, const LifeV::Real& /* y */,
                          const LifeV::Real& /* z */, const LifeV::ID&   /* ic */)
    {
        double lv=M_lv->getLocalVolatility(x,t);
        //return 0.5*0.2*0.2*x*x;
        //return 0.5*0.136*0.136*x*x;
        return 0.5*lv*lv*x*x;
    }
protected:
    FEOption::lvPtr_Type M_lv;
};

FEAsianArithmeticAverageStrikeCall::FEAsianArithmeticAverageStrikeCall (double lowAssetPrice, double highAssetPrice, LifeV::UInt numElements):
    FEOption(lowAssetPrice, highAssetPrice, numElements)
{

}

void FEAsianArithmeticAverageStrikeCall::price (double strike, double maturity, double riskFreeRate, double dividendYield, lvPtr_Type localVolatility, int numTimeSteps)
{
    M_K=strike;
    M_T=maturity;
    M_r=riskFreeRate;
    M_q=dividendYield;
    M_lv=localVolatility;
    double dt=maturity/static_cast<double> (numTimeSteps);
    
    M_solution.reset( new vector_Type(M_uFESpace->map(), Unique) );
    
    // Create the functors for B.C. and I.C.
    PayoffAsianArithmeticAverageStrikeCall payoff(M_T);
    RobinMassCoeffBCFct robinMassCoeffBCFct(dt);
    
    // Functor that will return local volatility: sigma(x,t)
    Sigma sigma(M_lv);
    
    AsianArithmeticAverageStrikeDiffusionCoeff diffCoeff(M_lv);

    VectorSmall<1> oneVector;
    oneVector[0]=1.0;
    
    // Create the vector containing the solution obtained
    // at the previous step and initialize it to (S-K)+
    vectorPtr_Type solutionOld ( new vector_Type(M_uFESpace->map(), Repeated) );
    *solutionOld *= 0.0;
    M_uFESpace->interpolate ( static_cast<feSpace_Type::function_Type> (payoff), *solutionOld, 0.0 );
    
    for (int i=numTimeSteps-1; i>=0; --i)
    {
    RobinRhsBCFct robinRhsBCFct(dt,solutionOld);

        vector_Type assetPriceInterpolated (M_uFESpace->map(), Repeated);
        M_uFESpace->interpolate ( static_cast<feSpace_Type::function_Type> (assetPriceFct), assetPriceInterpolated, i*dt );

        vector_Type lvInterpolated (M_uFESpace->map(), Repeated);
        M_uFESpace->interpolate ( static_cast<feSpace_Type::function_Type> (sigma), lvInterpolated, i*dt );

        vector_Type diffCoeffInterpolated (M_uFESpace->map(), Repeated);
        M_uFESpace->interpolate ( static_cast<feSpace_Type::function_Type> (diffCoeff), diffCoeffInterpolated, i*dt );

        // Build the matrix and vectors
        boost::shared_ptr<matrix_Type> systemMatrix (new matrix_Type ( M_uFESpace->map() ) );
        *systemMatrix *= 0.0;
        vector_Type rhs (M_uFESpace->map(), Repeated);
        rhs *= 0.0;
        
        {
            using namespace ExpressionAssembly;
    
            integrate ( elements (M_uETFESpace->mesh() ),
                        //M_uFESpace->qr(),
                        quadRuleSeg3pt,
                        M_uETFESpace,
                        M_uETFESpace,
    
                        //0.5*(
                        -1.0* value(M_uETFESpace, diffCoeffInterpolated)*dot ( grad (phi_i) , grad (phi_j) )
                        - dot ( grad (phi_j), grad(M_uETFESpace, diffCoeffInterpolated) ) * phi_i
                        + dot ( grad (phi_j) , value(oneVector) ) * (1 - value (M_uETFESpace, assetPriceInterpolated) * value(M_r-M_q)) *phi_i
                        - value(M_q)*phi_j*phi_i //)
                        - phi_i * phi_j / value(dt)
    
                      )
                    >> systemMatrix;
    
            integrate ( elements (M_uETFESpace->mesh() ),
                        //M_uFESpace->qr(),
                        quadRuleSeg3pt,
                        M_uETFESpace,
    
                        //-0.5*(
                        //-1.0* value(M_uETFESpace, diffCoeffInterpolated)*dot ( grad (phi_i) , grad (M_uETFESpace, *solutionOld) )
                        //- dot ( grad (M_uETFESpace, *solutionOld), grad(M_uETFESpace, diffCoeffInterpolated) ) * phi_i
                        //+ dot ( grad (M_uETFESpace, *solutionOld) , value(oneVector) ) * (1 - value (M_uETFESpace, assetPriceInterpolated) * value(M_r-M_q)) *phi_i
                        //- value(M_q)*value (M_uETFESpace, *solutionOld)*phi_i )
                        value (M_uETFESpace, *solutionOld) * phi_i / value(-dt) 
    
                      )
                    >> rhs;
        }

        systemMatrix->globalAssemble();
        rhs.globalAssemble();

        BCHandler bchandler;

        //BCFunctionRobin leftBC ( static_cast<feSpace_Type::function_Type> ( robinRhsBCFct ), static_cast<feSpace_Type::function_Type> ( robinMassCoeffBCFct ) );
        BCFunctionBase rightBC ( static_cast<feSpace_Type::function_Type> ( zeroFct ) );
        vector_Type neumannBCVector (M_uFESpace->map(), Repeated);
        neumannBCVector=diffCoeffInterpolated;
        //neumannBCVector*=-1;
        BCVector leftBC(neumannBCVector,M_uFESpace->dof().numTotalDof(),0);

        bchandler.addBC ("Neumann", Structured1DLabel::LEFT, Natural, Full, leftBC, 1);
        //bchandler.addBC ("Robin", Structured1DLabel::LEFT, Robin, Full, leftBC, 1);
        bchandler.addBC ("Dirichlet", Structured1DLabel::RIGHT, Essential, Full, rightBC, 1);

        bchandler.bcUpdate (*M_uFESpace->mesh(), M_uFESpace->feBd(), M_uFESpace->dof() );
        vectorPtr_Type rhsBC( new vector_Type (rhs, Unique) );
        bcManage (*systemMatrix, *rhsBC, *M_uFESpace->mesh(), M_uFESpace->dof(), bchandler, M_uFESpace->feBd(), 1.0, i*dt);
        rhs = *rhsBC;

        // Solve the linear problem
        std::cout << "Solving linear system at iteration " << i << "for t=" << i*dt << std::endl;
        M_linearSolver.setOperator (systemMatrix);
        M_linearSolver.setRightHandSide (rhsBC);
        M_linearSolver.solve (M_solution);
        
        *solutionOld=*M_solution;
    }
    //std::cout << "------> " << M_uFESpace->mesh()->element ( 0 ).point (0).coordinate (0) << std::endl;
}
