#ifndef _FEBARRIERUPANDOUTCALL_HPP
#define _FEBARRIERUPANDOUTCALL_HPP

#include "FEOption.hpp"

class FEBarrierUpAndOutCall: public FEOption
{
public:
    FEBarrierUpAndOutCall ();
    FEBarrierUpAndOutCall (double lowAssetPrice, double highAssetPrice, LifeV::UInt numElements);
    
    void price (double strike, double maturity, double riskFreeRate, double dividendYield, lvPtr_Type localVolatility, int numTimeSteps);
    
protected:

};

#endif
