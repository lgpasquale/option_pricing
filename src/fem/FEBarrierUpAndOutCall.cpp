#include "FEBarrierUpAndOutCall.hpp"

#include <lifev/eta/expression/Integrate.hpp>

using namespace LifeV;

class PayoffBarrierUpAndOutCall
{
public:
    PayoffBarrierUpAndOutCall (double strike) : M_K(strike) {}
    Real operator() ( const Real& /* t */, const Real& x, const Real& /* y */,
                          const Real& /* z */, const ID&   /* ic */)
    {
        return std::max( x-M_K , 0.0);
    }
protected:
    double M_K;
};

class BarrierUpAndOutCallDiffusionCoeff
{
public:
    BarrierUpAndOutCallDiffusionCoeff (FEOption::lvPtr_Type localVolatility) : M_lv(localVolatility) {}
    LifeV::Real operator() ( const LifeV::Real& t, const LifeV::Real& x, const LifeV::Real& /* y */,
                          const LifeV::Real& /* z */, const LifeV::ID&   /* ic */)
    {
        double lv=M_lv->getLocalVolatility(x,t);
        return 0.5*lv*lv*x*x;
        //return M_lv->getLocalVolatility(std::exp(x),t);
    }
protected:
    FEOption::lvPtr_Type M_lv;
};

FEBarrierUpAndOutCall::FEBarrierUpAndOutCall (double lowAssetPrice, double highAssetPrice, LifeV::UInt numElements):
    FEOption(lowAssetPrice, highAssetPrice, numElements)
{

}

void FEBarrierUpAndOutCall::price (double strike, double maturity, double riskFreeRate, double dividendYield, lvPtr_Type localVolatility, int numTimeSteps)
{
    M_K=strike;
    M_T=maturity;
    M_r=riskFreeRate;
    M_q=dividendYield;
    M_lv=localVolatility;
    double dt=maturity/static_cast<double> (numTimeSteps);
    
    M_solution.reset( new vector_Type(M_uFESpace->map(), Unique) );
    
    // Create the functor for I.C.
    PayoffBarrierUpAndOutCall payoff(M_K);
    
    // Functor that will return local volatility: sigma(x,t)
    Sigma sigma(M_lv);
    
    BarrierUpAndOutCallDiffusionCoeff diffCoeff(M_lv);
    
    VectorSmall<1> oneVector;
    oneVector[0]=1.0;
    
    // Create the vector containing the solution obtained
    // at the previous step and initialize it to (S-K)+
    vector_Type solutionOld (M_uFESpace->map(), Repeated);
    solutionOld *= 0.0;
    M_uFESpace->interpolate ( static_cast<feSpace_Type::function_Type> (payoff), solutionOld, 0.0 );
    
    for (int i=numTimeSteps-1; i>=0; --i)
    //for (int i=numTimeSteps-1; i>=50; --i)
    {
        vector_Type assetPriceInterpolated (M_uFESpace->map(), Repeated);
        M_uFESpace->interpolate ( static_cast<feSpace_Type::function_Type> (assetPriceFct), assetPriceInterpolated, i*dt );

        vector_Type lvInterpolated (M_uFESpace->map(), Repeated);
        M_uFESpace->interpolate ( static_cast<feSpace_Type::function_Type> (sigma), lvInterpolated, i*dt );

        vector_Type diffCoeffInterpolated (M_uFESpace->map(), Repeated);
        M_uFESpace->interpolate ( static_cast<feSpace_Type::function_Type> (diffCoeff), diffCoeffInterpolated, i*dt );

        // Build the matrix and vectors
        boost::shared_ptr<matrix_Type> systemMatrix (new matrix_Type ( M_uFESpace->map() ) );
        *systemMatrix *= 0.0;
        vector_Type rhs (M_uFESpace->map(), Repeated);
        rhs *= 0.0;
        
        {
            using namespace ExpressionAssembly;
    
            integrate ( elements (M_uETFESpace->mesh() ),
                        M_uFESpace->qr(),
                        M_uETFESpace,
                        M_uETFESpace,
    
                        0.5*(
                        -1.0*value(M_uETFESpace, diffCoeffInterpolated)*dot ( grad (phi_i) , grad (phi_j) )
                        - dot ( grad (phi_j), grad(M_uETFESpace, diffCoeffInterpolated) ) * phi_i
                        + dot ( grad (phi_j) , value(oneVector) ) * value (M_uETFESpace, assetPriceInterpolated) * value(M_r-M_q)  *phi_i
                        - value(M_r) * phi_i * phi_j )
                        - phi_i * phi_j / value(dt)
    
                      )
                    >> systemMatrix;
    
            integrate ( elements (M_uETFESpace->mesh() ),
                        M_uFESpace->qr(),
                        M_uETFESpace,
    
                        value (M_uETFESpace, solutionOld) * phi_i / value(-dt) 
                        -0.5*(
                        -1.0*value(M_uETFESpace, diffCoeffInterpolated)*dot ( grad (M_uETFESpace, solutionOld) , grad (phi_i) )
                        - dot ( grad (M_uETFESpace, solutionOld), grad(M_uETFESpace, diffCoeffInterpolated) ) * phi_i
                        + dot ( grad (M_uETFESpace, solutionOld) , value(oneVector) ) * value (M_uETFESpace, assetPriceInterpolated) * value(M_r-M_q)  *phi_i
                        - value(M_r) * value (M_uETFESpace, solutionOld) * phi_i )
    
                      )
                    >> rhs;
        }

        systemMatrix->globalAssemble();
        rhs.globalAssemble();

        BCHandler bchandler;

        BCFunctionBase leftBC ( static_cast<feSpace_Type::function_Type> ( zeroFct ) );
        BCFunctionBase rightBC ( static_cast<feSpace_Type::function_Type> ( zeroFct ) );

        bchandler.addBC ("Dirichlet", Structured1DLabel::LEFT, Essential, Full, leftBC, 1);
        bchandler.addBC ("Dirichlet", Structured1DLabel::RIGHT, Essential, Full, rightBC, 1);

        bchandler.bcUpdate (*M_uFESpace->mesh(), M_uFESpace->feBd(), M_uFESpace->dof() );
        vectorPtr_Type rhsBC( new vector_Type (rhs, Unique) );
        bcManage (*systemMatrix, *rhsBC, *M_uFESpace->mesh(), M_uFESpace->dof(), bchandler, M_uFESpace->feBd(), 1.0, i*dt);
        rhs = *rhsBC;

        // Solve the linear problem
        std::cout << "Solving linear system at iteration " << i << "for t=" << i*dt << std::endl;
        M_linearSolver.setOperator (systemMatrix);
        M_linearSolver.setRightHandSide (rhsBC);
        M_linearSolver.solve (M_solution);
        
        solutionOld=*M_solution;
    }
}
