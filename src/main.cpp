#include <lifev/core/LifeV.hpp>
#include <lifev/core/util/LifeChrono.hpp>
#include <iostream>

#include "volatility/MarketData.hpp"
#include "volatility/LocalVolatility.hpp"
#include "fem/FEEuropeanCall.hpp"
#include "fem/FEAmericanPut.hpp"
#include "fem/FEBarrierUpAndOutCall.hpp"
#include "fem/FEAsianArithmeticAverageStrikeCall.hpp"

int main (int argc, char** argv)
{
#ifdef HAVE_MPI
    MPI_Init (&argc, &argv);
    int rank;
    int size;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Comm_size(MPI_COMM_WORLD,&size);
#endif

    boost::shared_ptr<MarketData> marketData( new MarketData("calls_chain") );
    marketData->computeMarketIV();std::cout<<"done"<<std::endl;
    boost::shared_ptr<LocalVolatility> volatility( new LocalVolatility(marketData,80,80) );std::cout<<"done"<<std::endl;

    marketData->exportMarketIV("marketIV");
    volatility->exportLV("LV");
    volatility->exportMarketLV("marketLV");
    
    LifeV::LifeChrono chrono;
    chrono.start();

    //================================
    // European Call
    //================================
    FEEuropeanCall feEuropeanCall(marketData->getAssetPrice()*0.01,marketData->getAssetPrice()*5.0,250);
    
    std::vector<std::vector<double> > euPrices(marketData->getNumMaturities(), std::vector<double>(marketData->getNumStrikes()) );
    std::vector<std::vector<double> > priceErrors(marketData->getNumMaturities(), std::vector<double>(marketData->getNumStrikes()) );
    std::vector<std::vector<double> > bsPriceErrors(marketData->getNumMaturities(), std::vector<double>(marketData->getNumStrikes()) );
    for (int i=0; i<marketData->getNumMaturities(); ++i)
    {
        for (int j=0; j<marketData->getNumStrikes(); ++j)
        {
            if (rank != j%size)
                continue;

            feEuropeanCall.price(marketData->getStrike(j),marketData->getMaturity(i),marketData->getRiskFreeRate(),marketData->getDividendYield(),volatility,
                std::ceil(marketData->getMaturity(i)/marketData->getMaturities().back()*80.0));
                
            euPrices[i][j] = feEuropeanCall.getPrice(marketData->getAssetPrice());

            if (i==0 && j==3)
                feEuropeanCall.exportSolutionToGnuplot("eu_call_03",250);
            if (i==6 && j==3)
                feEuropeanCall.exportSolutionToGnuplot("eu_call_63",250);
            if (i==9 && j==3)
                feEuropeanCall.exportSolutionToGnuplot("eu_call_93",250);
        }
    }
    
    boost::math::normal_distribution<> M_normalDist(0,1);

    
    for (int i=0; i<marketData->getNumMaturities(); ++i)
    {
        for (int j=0; j<marketData->getNumStrikes(); ++j)
        {
            int owner=j%size;
            MPI_Bcast(&euPrices[i][j], 1, MPI_DOUBLE, owner, MPI_COMM_WORLD);
            //priceErrors[i][j] = (euPrices[i][j]-marketData->getMarketCall(i,j))/marketData->getMarketCall(i,j);
            priceErrors[i][j] = (euPrices[i][j]-marketData->getMarketCall(i,j))/marketData->getAssetPrice();

            const double sigma = 0.136;
            double d1 = (std::log(marketData->getAssetPrice()/marketData->getStrike(j))+(marketData->getRiskFreeRate()-marketData->getDividendYield()+0.5*sigma*sigma)*marketData->getMaturity(i))/(sigma*std::sqrt(marketData->getMaturity(i)));
            double d2 = (std::log(marketData->getAssetPrice()/marketData->getStrike(j))+(marketData->getRiskFreeRate()-marketData->getDividendYield()-0.5*sigma*sigma)*marketData->getMaturity(i))/(sigma*std::sqrt(marketData->getMaturity(i)));
            double bsCall = marketData->getAssetPrice()*std::exp(-marketData->getDividendYield()*marketData->getMaturity(i))*cdf(M_normalDist, d1) - marketData->getStrike(j)*std::exp(-marketData->getRiskFreeRate()*marketData->getMaturity(i))*cdf(M_normalDist, d2);
            //bsPriceErrors[i][j] = (marketData->getMarketCall(i,j)-bsCall)/marketData->getMarketCall(i,j);
            bsPriceErrors[i][j] = (bsCall-marketData->getMarketCall(i,j))/marketData->getAssetPrice();
        }
    }
    
    if (rank==0)
        exportToGnuplot3d("eu_calls",marketData->getMaturities(),marketData->getStrikes(),euPrices);
    if (rank==0)
        exportToGnuplot3d("eu_errors",marketData->getMaturities(),marketData->getStrikes(),priceErrors);
    if (rank==0)
        exportToGnuplot3d("eu_bserrors",marketData->getMaturities(),marketData->getStrikes(),bsPriceErrors);
        
    chrono.stop();
    std::cerr << "Time: " << chrono.diff() << std::endl;

    //================================
    // Barrier Up And Out Call
    //================================
    FEBarrierUpAndOutCall feBarrierUpAndOutCall(marketData->getAssetPrice()*0.01,marketData->getStrikes().back()*1.5,250);

    std::vector<std::vector<double> > barrierPrices(marketData->getNumMaturities(), std::vector<double>(marketData->getNumStrikes()) );
    for (int i=0; i<marketData->getNumMaturities(); ++i)
    {
        for (int j=0; j<marketData->getNumStrikes(); ++j)
        {
            if (rank != j%size)
                continue;

            feBarrierUpAndOutCall.price(marketData->getStrike(j),marketData->getMaturity(i),marketData->getRiskFreeRate(),marketData->getDividendYield(),volatility,
                std::ceil(marketData->getMaturity(i)/marketData->getMaturities().back()*80.0));
                
            barrierPrices[i][j] = feBarrierUpAndOutCall.getPrice(marketData->getAssetPrice());

            if (i==0 && j==3)
                feBarrierUpAndOutCall.exportSolutionToGnuplot("buo_call_03",250);
            if (i==6 && j==3)
                feBarrierUpAndOutCall.exportSolutionToGnuplot("buo_call_63",250);
            if (i==9 && j==3)
                feBarrierUpAndOutCall.exportSolutionToGnuplot("buo_call_93",250);
        }
    }

    for (int i=0; i<marketData->getNumMaturities(); ++i)
    {
        for (int j=0; j<marketData->getNumStrikes(); ++j)
        {
            int owner=j%size;
            MPI_Bcast(&barrierPrices[i][j], 1, MPI_DOUBLE, owner, MPI_COMM_WORLD);
        }
    }
    if (rank==0)
        exportToGnuplot3d("buo_calls",marketData->getMaturities(),marketData->getStrikes(),barrierPrices);

    //================================
    // Asian Arithmetic Average Strike
    //================================
    FEAsianArithmeticAverageStrikeCall feAsianArithmeticAverageStrikeCall(0.0,marketData->getAssetPrice()*8.0,100);
    //FEAsianArithmeticAverageStrikeCall feAsianArithmeticAverageStrikeCall(0.0,10.0,1000);

    std::vector<double> asianPrices(marketData->getNumMaturities() );
    for (int i=0; i<marketData->getNumMaturities(); ++i)
    {
        if (rank != i%size)
            continue;
        //if (i!=0)
            //continue;

        double dtCFL = marketData->getMaturity(i)/100/std::sqrt(3.0);
        feAsianArithmeticAverageStrikeCall.price(marketData->getMaturity(i),marketData->getRiskFreeRate(),marketData->getDividendYield(),volatility,
            std::ceil(marketData->getMaturity(i)/dtCFL*1.2));
            //std::ceil(marketData->getMaturity(i)/marketData->getMaturities().back()*1000.0));
            
        asianPrices[i] = feAsianArithmeticAverageStrikeCall.getPrice(marketData->getAssetPrice());
        //asianPrices[i] = feAsianArithmeticAverageStrikeCall.getPrice(0.0);

        if (i==6)
        {
            feAsianArithmeticAverageStrikeCall.exportSolutionToGnuplot("aaas_call_6",250);
            //feAsianArithmeticAverageStrikeCall.exportSolution("call_1_1");
        }
    }

    for (int i=0; i<marketData->getNumMaturities(); ++i)
    {
        int owner=i%size;
        MPI_Bcast(&asianPrices[i], 1, MPI_DOUBLE, owner, MPI_COMM_WORLD);
    }
    if (rank==0)
        exportToGnuplot2d("aaas_calls",marketData->getMaturities(),asianPrices);

    //================================
    // American Put
    //================================
    FEAmericanPut feAmericanPut(marketData->getAssetPrice()*0.01,marketData->getAssetPrice()*5.0,100);

    std::vector<std::vector<double> > americanPrices(marketData->getNumMaturities(), std::vector<double>(marketData->getNumStrikes()) );
    for (int i=0; i<marketData->getNumMaturities(); ++i)
    {
        for (int j=0; j<marketData->getNumStrikes(); ++j)
        {
            if (rank != j%size)
                continue;

            feAmericanPut.price(marketData->getStrike(j),marketData->getMaturity(i),marketData->getRiskFreeRate(),marketData->getDividendYield(),volatility,
                std::ceil(marketData->getMaturity(i)/marketData->getMaturities().back()*100.0));
                
            americanPrices[i][j] = feAmericanPut.getPrice(marketData->getAssetPrice());

            if (i==0 && j==3)
                feAmericanPut.exportSolutionToGnuplot("american_put_03",250);
            if (i==6 && j==3)
                feAmericanPut.exportSolutionToGnuplot("american_put_63",250);
            if (i==9 && j==3)
                feAmericanPut.exportSolutionToGnuplot("american_put_93",250);
                //feAmericanPut.exportSolution("call_1_1");
        }
    }

    for (int i=0; i<marketData->getNumMaturities(); ++i)
    {
        for (int j=0; j<marketData->getNumStrikes(); ++j)
        {
            int owner=j%size;
            MPI_Bcast(&americanPrices[i][j], 1, MPI_DOUBLE, owner, MPI_COMM_WORLD);
        }
    }
    if (rank==0)
        exportToGnuplot3d("american_puts",marketData->getMaturities(),marketData->getStrikes(),americanPrices);
    

#ifdef HAVE_MPI
    MPI_Finalize();
#endif

    return 0;
}
