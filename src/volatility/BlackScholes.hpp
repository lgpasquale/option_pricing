#ifndef _BLACKSCHOLES_HPP
#define _BLACKSCHOLES_HPP

#include <iostream>
#include <cmath>

#include <boost/math/distributions.hpp>

namespace BlackScholes
{

double bsEuropeanCall(const double& assetPrice, const double& strike, const double& T, const double& r, const double& q, const double& sigma);
}

#endif
