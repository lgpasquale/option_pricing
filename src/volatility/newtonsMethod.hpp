#ifndef _NEWTONSMETHOD_HPP
#define _NEWTONSMETHOD_HPP

template <class F, class DF>
int newtonsMethod(F function, DF derivative, double& solution, const double initialGuess, const double tolerance, const int maxIter = 100)
{
    double xOld;
    double x=initialGuess;
    double error = 2.0*tolerance;
    int i=0;
    while (error > tolerance && i < maxIter)
    {
        xOld=x;
        x=xOld-function(xOld)/derivative(xOld);
        error = std::abs(x-xOld);
        i++;
    }
    solution=x;
    
    return i;
    
}

#endif
