#include "LocalVolatility.hpp"

LocalVolatility::LocalVolatility (boost::shared_ptr<MarketData> marketData, const double numInterpolatedMaturities, const double numInterpolatedStrikes):
    M_marketData(marketData),
    M_numMaturities(numInterpolatedMaturities), M_numStrikes(numInterpolatedStrikes),
    M_calls(numInterpolatedMaturities,std::vector<double>(numInterpolatedStrikes)),
    M_IV(numInterpolatedMaturities,std::vector<double>(numInterpolatedStrikes)),
    M_marketLV(marketData->getNumMaturities(),std::vector<double>(marketData->getNumStrikes())),
    M_LV(numInterpolatedMaturities,std::vector<double>(numInterpolatedStrikes)),
    M_splines(marketData->getNumStrikes()),
    M_currentSpline(),
    M_currentTime(-1)
{
    double assetPrice=M_marketData->getAssetPrice();
    double r=M_marketData->getRiskFreeRate();
    double q=M_marketData->getDividendYield();
    //================================
    // Market calls filtration
    //================================
    // We want to remove those calls that do not respect
    // the no-arbitrage conditions:
    // d2C/dK2 >= 0
    // dC/dK <= 0
    // dC/dT >= 0
    
    
    //================================
    // Calls interpolation
    //================================
    
    // First create the desired number of maturity and strike steps
    M_maturities.resize(M_numMaturities);
    M_strikes.resize(M_numStrikes);
    //M_maturityStepLenght = (M_marketData->getMaturities().back()-M_marketData->getMaturities().front())/(M_numMaturities-1);
    M_maturityStepLenght = (M_marketData->getMaturities().back()-0.0)/(M_numMaturities-1);
    M_strikeStepLenght = (M_marketData->getStrikes().back()-M_marketData->getStrikes().front())/(M_numStrikes-1);
    //M_strikeStepLenght = (M_marketData->getStrikes().back()*2.0-0.0)/(M_numStrikes-1);
    for (int i=0; i<M_numMaturities; ++i)
    {
        M_maturities[i]=i*M_maturityStepLenght;
    }
    for (int j=0; j<M_numStrikes; ++j)
    {
        M_strikes[j]=M_marketData->getStrike(0)+j*M_strikeStepLenght;
        //M_strikes[j]=0.0+j*M_strikeStepLenght;
    }
    
    
    //================================
    // Market Local Volatiliy
    //================================

    double n = M_marketData->getNumMaturities();
    double m = M_marketData->getNumStrikes();
    std::vector<std::vector<double> > dck (n, std::vector<double>(m));
    std::vector<std::vector<double> > d2ck (n, std::vector<double>(m));
    std::vector<std::vector<double> > dct (n, std::vector<double>(m));
    for (int i=0; i<M_marketData->getNumMaturities(); ++i)
    {
        for (int j=0; j<M_marketData->getNumStrikes(); ++j)
        {
            double dCK;
            double d2CK;
            double dCT;
            if (j==0)
            {
                double h1=M_marketData->getStrike(j)-0.;
                double h2=M_marketData->getStrike(j+1)-M_marketData->getStrike(j);
                dCK = -h2/h1/(h1+h2)*assetPrice*std::exp(r*M_marketData->getMaturity(i))
                    -(h1-h2)/h1/h2*M_marketData->getMarketCall(i,j)
                    +h1/(h1+h2)/h2*M_marketData->getMarketCall(i,j+1);
                d2CK = 2.*(h2*assetPrice*std::exp(r*M_marketData->getMaturity(i))
                    -(h1+h2)*M_marketData->getMarketCall(i,j)
                    +h1*M_marketData->getMarketCall(i,j+1))
                    /(h1*h2*(h1+h2));
            }
            else if (j==m-1)
            {
                double h1=M_marketData->getStrike(m-2)-M_marketData->getStrike(m-3);
                double h2=M_marketData->getStrike(m-1)-M_marketData->getStrike(m-2);
                dCK = -1./h2*M_marketData->getMarketCall(i,m-2)
                    +1./h2*M_marketData->getMarketCall(i,m-1);
                d2CK = 2.*(h2*M_marketData->getMarketCall(i,m-3)
                    -(h1+h2)*M_marketData->getMarketCall(i,m-2)
                    +h1*M_marketData->getMarketCall(i,m-1))
                    /(h1*h2*(h1+h2));
            }
            else
            {
                double h1=M_marketData->getStrike(j)-M_marketData->getStrike(j-1);
                double h2=M_marketData->getStrike(j+1)-M_marketData->getStrike(j);
                dCK = -h2/h1/(h1+h2)*M_marketData->getMarketCall(i,j-1)
                    -(h1-h2)/h1/h2*M_marketData->getMarketCall(i,j)
                    +h1/(h1+h2)/h2*M_marketData->getMarketCall(i,j+1);
                d2CK = 2.*(h2*M_marketData->getMarketCall(i,j-1)
                    -(h1+h2)*M_marketData->getMarketCall(i,j)
                    +h1*M_marketData->getMarketCall(i,j+1))
                    /(h1*h2*(h1+h2));
            }

            if (i==0)
            {
                double h1=M_marketData->getMaturity(i)-0;
                double h2=M_marketData->getMaturity(i+1)-M_marketData->getMaturity(i);
                dCT = -h2/h1/(h1+h2)*std::max(assetPrice-M_marketData->getStrike(j),0.0)
                    -(h1-h2)/h1/h2*M_marketData->getMarketCall(i,j)
                    +h1/(h1+h2)/h2*M_marketData->getMarketCall(i+1,j);
            }
            else if (i==n-1)
            {
                double h1=M_marketData->getMaturity(n-2)-M_marketData->getMaturity(n-3);
                double h2=M_marketData->getMaturity(n-1)-M_marketData->getMaturity(n-2);
                dCT = -1./h2*M_marketData->getMarketCall(n-2,j)
                    +1./h2*M_marketData->getMarketCall(n-1,j);
            }
            else
            {
                double h1=M_marketData->getMaturity(i)-M_marketData->getMaturity(i-1);
                double h2=M_marketData->getMaturity(i+1)-M_marketData->getMaturity(i);
                dCT = -h2/h1/(h1+h2)*M_marketData->getMarketCall(i-1,j)
                    -(h1-h2)/h1/h2*M_marketData->getMarketCall(i,j)
                    +h1/(h1+h2)/h2*M_marketData->getMarketCall(i+1,j);
            }
            dck[i][j]=dCK;
            d2ck[i][j]=d2CK;
            dct[i][j]=dCT;
            
            M_marketLV[i][j]= std::sqrt(std::max( (dCT+(r-q)*M_marketData->getStrike(j)*dCK+q*M_marketData->getMarketCall(i,j))/
                (0.5*M_marketData->getStrike(j)*M_marketData->getStrike(j)*d2CK) , -100.0) );
        }
    }
    
    std::vector<std::vector<double> > extendedMarketLV(M_marketData->getNumMaturities(), std::vector<double>(M_marketData->getNumStrikes()) );
    extendedMarketLV=M_marketLV;
    std::vector<double> marketLV0(M_marketData->getNumStrikes() );
    for (int j=0; j<M_marketData->getNumStrikes(); ++j)
    {
        marketLV0[j]=M_marketData->getMarketIV(0,j);
    }
    extendedMarketLV.insert(extendedMarketLV.begin(),marketLV0);
    std::vector<double> extendedMaturities(M_marketData->getNumMaturities() );
    extendedMaturities=M_marketData->getMaturities();
    extendedMaturities.insert(extendedMaturities.begin(),0.0);
    
    exportToGnuplot3d("dck",M_marketData->getMaturities(),M_marketData->getStrikes(),dck);
    exportToGnuplot3d("d2ck",M_marketData->getMaturities(),M_marketData->getStrikes(),d2ck);
    exportToGnuplot3d("dct",M_marketData->getMaturities(),M_marketData->getStrikes(),dct);

    //================================
    // Thin Plate Spline
    //================================
    std::vector<boost::array<double, 2> > positions;
    std::vector<boost::array<double, 1> > values;

    positions.reserve(M_marketData->getNumMaturities()*M_marketData->getNumStrikes());
    values.reserve(M_marketData->getNumMaturities()*M_marketData->getNumStrikes());
    
    for (int i=0; i<M_marketData->getNumMaturities(); ++i)
    {
        for (int j=0; j<M_marketData->getNumStrikes(); ++j)
        {
            boost::array<double, 2> curPos;
            curPos[0] = M_marketData->getMaturity(i); curPos[1] = M_marketData->getStrike(j)/assetPrice;
            positions.push_back(curPos);
        
            boost::array<double,1> curVal;
            curVal[0] = M_marketLV[i][j];
            values.push_back(curVal);
            
            M_splines[j].addPoint(M_marketData->getMaturity(i),M_marketLV[i][j]);
        }
    }

    M_spline.reset( new ThinPlateSpline<2,1>(positions,values) );
    
    //M_bilinearInterpolation.reset( new BilinearInterpolation(M_marketData->getMaturities(),M_marketData->getStrikes(),M_marketLV) );
    M_bilinearInterpolation.reset( new BilinearInterpolation(extendedMaturities,M_marketData->getStrikes(),extendedMarketLV) );

    std::cout << "Extrapolating local volatility surface... " << std::flush;
    std::vector<std::vector<double> > dLV(numInterpolatedMaturities,std::vector<double>(numInterpolatedStrikes));
    std::vector<std::vector<double> > d2LV(numInterpolatedMaturities,std::vector<double>(numInterpolatedStrikes));
    for (int i=0; i<M_numMaturities; ++i)
    {
        for (int j=0; j<M_numStrikes; ++j)
        {
            boost::array<double, 2> curPos;
            curPos[0] = M_maturities[i];
            curPos[1] = M_strikes[j]/assetPrice;
            //M_LV[i][j]=M_spline->interpolate(curPos)[0];
            M_LV[i][j]=getLocalVolatility(M_strikes[j],M_maturities[i]);
            dLV[i][j]=getLocalVolatilityFirstDerivative(M_strikes[j],M_maturities[i]);
            d2LV[i][j]=getLocalVolatilitySecondDerivative(M_strikes[j],M_maturities[i]);
        }
    }
    exportToGnuplot3d("dlv",M_maturities,M_strikes,dLV);
    exportToGnuplot3d("d2lv",M_maturities,M_strikes,d2LV);
    std::cout << "done." << std::endl;
    
}

void LocalVolatility::exportCalls(const char* filename)
{
    exportToGnuplot3d(filename,M_maturities,M_strikes,M_calls);
}

void LocalVolatility::exportIV(const char* filename)
{
    exportToGnuplot3d(filename,M_maturities,M_strikes,M_IV);
}

void LocalVolatility::exportMarketLV(const char* filename)
{
    exportToGnuplot3d(filename,M_marketData->getMaturities(),M_marketData->getStrikes(),M_marketLV);
}

void LocalVolatility::exportLV(const char* filename)
{
    exportToGnuplot3d(filename,M_maturities,M_strikes,M_LV);
}

double LocalVolatility::getLocalVolatility (int strikeIndex, int maturityIndex)
{
    return M_LV[maturityIndex][strikeIndex];
}

void LocalVolatility::updateCurrentSpline(const double& t)
{
    double t2=t;
    if (t < M_marketData->getMaturities().front())
    {
        t2 = M_marketData->getMaturities().front();
    }
    if (t > M_marketData->getMaturities().back())
    {
        t2 = M_marketData->getMaturities().back();
    }
    M_currentTime = t;
    M_currentSpline.clear();
    double h1 = M_marketData->getStrike(1)-M_marketData->getStrike(0);
    double h0 = h1*1.e-4;
    M_currentSpline.addPoint(M_marketData->getStrike(0)+h0,M_splines.front()(t2));
    for (int j=0; j<M_marketData->getNumStrikes(); ++j)
    {
        M_currentSpline.addPoint(M_marketData->getStrike(j),M_splines[j](t2));
    }
    double hn=M_marketData->getStrike(M_marketData->getNumStrikes()-1)-M_marketData->getStrike(M_marketData->getNumStrikes()-2);
    double hm=hn*1.e-4;
    M_currentSpline.addPoint(M_marketData->getStrikes().back()-hm,M_splines.back()(t2));
}

double LocalVolatility::getLocalVolatility (double assetPrice, double t)
{
    boost::array<double, 2> curPos;
    curPos[0] = t;
    //curPos[1] = assetPrice/M_marketData->getAssetPrice();
    curPos[1] = assetPrice;
    if ( t > M_marketData->getMaturities().back() )
    {
        curPos[0] = M_marketData->getMaturities().back();
    }
    if ( t < M_marketData->getMaturities().front() )
    //if ( t < *(M_marketData->getMaturities().begin()+1) )
    {
        //curPos[0] = M_marketData->getMaturities().front();
        //curPos[0] = *(M_marketData->getMaturities().begin()+1);
    }
    if ( assetPrice > M_marketData->getStrikes().back() )
    //if ( assetPrice > *(M_marketData->getStrikes().end()-2) )
    //if ( assetPrice > M_marketData->getStrikes().back() + (M_marketData->getStrikes().back()-M_marketData->getStrikes().front())*0.1 )
    {
        curPos[1] = M_marketData->getStrikes().back();
        //curPos[1] = *(M_marketData->getStrikes().end()-2);
        //curPos[1] = ( M_marketData->getStrikes().back() + (M_marketData->getStrikes().back()-M_marketData->getStrikes().front())*0.1 )/M_marketData->getAssetPrice();
    }
    if ( assetPrice < M_marketData->getStrikes().front() )
    //if ( assetPrice < *(M_marketData->getStrikes().begin()+1) )
    //if ( assetPrice < M_marketData->getStrikes().front() - (M_marketData->getStrikes().back()-M_marketData->getStrikes().front())*0.1 )
    {
        curPos[1] = M_marketData->getStrikes().front();
        //curPos[1] = *(M_marketData->getStrikes().begin()+1);
        //curPos[1] = (M_marketData->getStrikes().front() - (M_marketData->getStrikes().back()-M_marketData->getStrikes().front())*0.1 )/M_marketData->getAssetPrice();
    }

    //return M_spline->interpolate(curPos)[0];
    //return 0.05+0.01*assetPrice;
    //return 0.2;
    //return 0.136;

    //if (M_currentTime != t)
    //{
        //updateCurrentSpline(t);
    //}
    //return M_currentSpline(curPos[1]);

    //if (t<M_marketData->getMaturity(0))
        //return (10.+(M_splines[0](assetPrice)-10.)*t);
    //for (int i=0; i<M_marketData->getNumMaturities(); ++i)
    //{
        //if (t<M_marketData->getMaturity(i))
        //{
            //return M_splines[i-1](assetPrice)+(M_splines[i](assetPrice)-M_splines[i-1](assetPrice))*(t-M_marketData->getMaturity(i-1))/(M_marketData->getMaturity(i)-M_marketData->getMaturity(i-1));
        //}
    //}
    //return M_splines.back()(assetPrice);
    
    return std::min(M_bilinearInterpolation->evaluate(curPos[0],curPos[1]),100.0);


}

double LocalVolatility::getLocalVolatilityFirstDerivative (double assetPrice, double t)
{
    boost::array<double, 2> curPos;
    curPos[0] = t;
    curPos[1] = assetPrice;
    if ( t > M_marketData->getMaturities().back() )
    {
        curPos[0] = M_marketData->getMaturities().back();
    }
    if ( t < M_marketData->getMaturities().front() )
    {
        curPos[0] = M_marketData->getMaturities().front();
    }
    if ( assetPrice > M_marketData->getStrikes().back() )
    {
        return 0.;
    }
    if ( assetPrice < M_marketData->getStrikes().front() )
    {
        return 0.;
    }

    if (M_currentTime != t)
    {
        updateCurrentSpline(t);
    }
    return M_currentSpline.firstDerivative(assetPrice);

}

double LocalVolatility::getLocalVolatilitySecondDerivative (double assetPrice, double t)
{
    boost::array<double, 2> curPos;
    curPos[0] = t;
    curPos[1] = assetPrice;
    if ( t > M_marketData->getMaturities().back() )
    {
        curPos[0] = M_marketData->getMaturities().back();
    }
    if ( t < M_marketData->getMaturities().front() )
    {
        curPos[0] = M_marketData->getMaturities().front();
    }
    if ( assetPrice > M_marketData->getStrikes().back() )
    {
        return 0.;
    }
    if ( assetPrice < M_marketData->getStrikes().front() )
    {
        return 0.;
    }

    if (M_currentTime != t)
    {
        updateCurrentSpline(t);
    }
    return M_currentSpline.secondDerivative(assetPrice);

}

double LocalVolatility::getLocalVolatilityIntegral (double assetPrice1, double assetPrice2, double t)
{
    double sl=std::min(assetPrice1,assetPrice2);
    double sr=std::max(assetPrice1,assetPrice2);
    double integral=0;
    boost::array<double, 2> curPos;
    curPos[0] = t;
    if ( t > M_marketData->getMaturities().back() )
    {
        curPos[0] = M_marketData->getMaturities().back();
    }
    if ( t < M_marketData->getMaturities().front() )
    {
        curPos[0] = M_marketData->getMaturities().front();
    }

    if (M_currentTime != t)
    {
        std::cout << "New time: " << t << std::endl;
        M_currentTime = t;
        M_currentSpline.clear();
        for (int j=0; j<M_marketData->getNumStrikes(); ++j)
        {
            M_currentSpline.addPoint(M_marketData->getStrike(j),M_splines[j](curPos[0]));
        }
    }
    
    if ( sl < M_marketData->getStrikes().front() )
    {
        integral -= (sl-M_marketData->getStrikes().front()) * M_currentSpline(M_marketData->getStrikes().front());
    }
    if ( sl > M_marketData->getStrikes().back() )
    {
        integral -= (sl-M_marketData->getStrikes().back()) * M_currentSpline(M_marketData->getStrikes().back());
    }
    if ( sr < M_marketData->getStrikes().front() )
    {
        integral += (sr-M_marketData->getStrikes().front()) * M_currentSpline(M_marketData->getStrikes().front());
    }
    if ( sr < M_marketData->getStrikes().back() )
    {
        integral += (sr-M_marketData->getStrikes().back()) * M_currentSpline(M_marketData->getStrikes().back());
    }
    if ( sl < M_marketData->getStrikes().back() && sr > M_marketData->getStrikes().front() )
    {
        integral += M_currentSpline.integrate( std::max(sl,M_marketData->getStrikes().front()), std::min(sr,M_marketData->getStrikes().back()) );
    }
    if (assetPrice1>assetPrice2)
    {
        integral*=-1;
    }
    return integral;

}

void LocalVolatility::filterMonotonePoints(const std::vector<double>& y, std::vector<bool>& selectedPoints, bool increasing)
{
    unsigned int n=y.size();
    if (selectedPoints.size()<n)
    {
        selectedPoints.resize(n);
    }
    
    std::vector<std::vector<int> > monotonicityMatrix(n, std::vector<int>(n,0) );
    for (unsigned int i=0; i<n; ++i)
    {
        for (unsigned int j=i+1; j<n; ++j)
        {
            if ( (increasing)?(y[i]>y[j]):(y[i]<y[j]) )
            {
                monotonicityMatrix[i][j]=1;
                monotonicityMatrix[j][i]=1;
            }
        }
    }
    
    std::fill(selectedPoints.begin(),selectedPoints.end(),true);
    
    std::vector<int> violatedConstraints(n,0);
    // Matrix vector multiplication
    for (unsigned int i=0; i<n; ++i)
    {
        for (unsigned int j=0; j<n; ++j)
        {
            violatedConstraints[i]+=monotonicityMatrix[i][j];
        }
    }
    
    // Find the point that violates the most constraints
    int m=std::distance(violatedConstraints.begin(), std::max_element(violatedConstraints.begin(),violatedConstraints.end()) );

    while (violatedConstraints[m]>0)
    {
        // We won't be using this point anymore
        selectedPoints[m]=false;
        // so it doesn't matter if it violates any constraints
        violatedConstraints[m]=0;
        // All the other points have one less constraint to satisfy as well
        for (unsigned int i=0; i<n; ++i)
        {
            violatedConstraints[i]-=monotonicityMatrix[i][m];
        }
        
        // Find the next point that violates the most constraints
        m=std::distance(violatedConstraints.begin(), std::max_element(violatedConstraints.begin(),violatedConstraints.end()) );
    }
}

void LocalVolatility::filterConvexPoints(const std::vector<double>& x, const std::vector<double>& y, std::vector<bool>& selectedPoints)
{
    unsigned int n=x.size();
    if (selectedPoints.size()<n)
    {
        selectedPoints.resize(n);
    }

    std::vector<std::vector<std::vector<int> > > convexityMatrix(n, std::vector<std::vector<int> >(n, std::vector<int>(n, 0) ) );
    for (unsigned int i=0; i<n; ++i)
    {
        for (unsigned int j=i+1; j<n; ++j)
        {
            for (unsigned int k=j+1; k<n; ++k)
            {
                if ( (y[j]-y[i])/(x[j]-x[i]) > (y[k]-y[j])/(x[k]-x[j]) )
                {
                    convexityMatrix[i][j][k]=1;
                    convexityMatrix[i][k][j]=1;
                    convexityMatrix[j][i][k]=1;
                    convexityMatrix[j][k][i]=1;
                    convexityMatrix[k][i][j]=1;
                    convexityMatrix[k][j][i]=1;
                }
            }
        }
    }
    
    std::fill(selectedPoints.begin(),selectedPoints.end(),true);
    
    std::vector<int> violatedConstraints(n,0);
    // Matrix vector multiplication
    for (unsigned int i=0; i<n; ++i)
    {
        for (unsigned int j=0; j<n; ++j)
        {
            for (unsigned int k=0; k<n; ++k)
            {
                violatedConstraints[i]+=convexityMatrix[i][j][k];
            }
        }
    }
    
    // Find the point that violates the most constraints
    int m=std::distance(violatedConstraints.begin(), std::max_element(violatedConstraints.begin(),violatedConstraints.end()) );

    while (violatedConstraints[m]>0)
    {
        // We won't be using this point anymore
        selectedPoints[m]=false;
        // so it doesn't matter if it violates any constraints
        violatedConstraints[m]=0;
        // All the other points have one less constraint to satisfy as well
        for (unsigned int i=0; i<n; ++i)
        {
            for (unsigned int j=0; j<n; ++j)
            {
                violatedConstraints[i]-=convexityMatrix[i][j][m];
                violatedConstraints[i]-=convexityMatrix[i][m][j];
            }
        }
        
        // Find the next point that violates the most constraints
        m=std::distance(violatedConstraints.begin(), std::max_element(violatedConstraints.begin(),violatedConstraints.end()) );
    }

}

