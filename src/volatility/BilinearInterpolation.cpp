#include "BilinearInterpolation.hpp"

BilinearInterpolation::BilinearInterpolation(const std::vector<double>& x, const std::vector<double>& y, const std::vector<std::vector<double> >& z):
    M_x(x), M_y(y), M_a(x.size()-1, std::vector<std::vector<double> >(y.size()-1,std::vector<double>(4)) )
{
    if (x.size()<2 || y.size()<2)
    {
        std::cout << "Must have at least two values in each direction to interpolate" << std::endl;
    }
    
    for (unsigned int i=0; i<x.size()-1; ++i)
    {
        for (unsigned int j=0; j<y.size()-1; ++j)
        {
            M_a[i][j][0]=(x[i+1]*y[j+1]*z[i][j]-x[i]*y[j+1]*z[i+1][j]-x[i+1]*y[j]*z[i][j+1]+x[i]*y[j]*z[i+1][j+1])/(x[i+1]-x[i])/(y[j+1]-y[j]);
            M_a[i][j][1]=(-y[j+1]*z[i][j]+y[j+1]*z[i+1][j]+y[j]*z[i][j+1]-y[j]*z[i+1][j+1])/(x[i+1]-x[i])/(y[j+1]-y[j]);
            M_a[i][j][2]=(-x[i+1]*z[i][j]+x[i]*z[i+1][j]+x[i+1]*z[i][j+1]-x[i]*z[i+1][j+1])/(x[i+1]-x[i])/(y[j+1]-y[j]);
            M_a[i][j][3]=(z[i][j]-z[i+1][j]-z[i][j+1]+z[i+1][j+1])/(x[i+1]-x[i])/(y[j+1]-y[j]);
        }
    }
    
}

double BilinearInterpolation::evaluate(const double& x, const double& y)
{
    std::vector<double>::iterator up;
    // Find in which interval the coordinates are
    // Along the X axis
    up = std::upper_bound (M_x.begin(), M_x.end(), x);
    unsigned int i=std::distance(M_x.begin(),up)-1;
    // Along the Y axis
    up = std::upper_bound (M_y.begin(), M_y.end(), y);
    unsigned int j=std::distance(M_y.begin(),up)-1;

    if (i<0)
    {
        i=0;
    }
    if (i>=M_x.size()-1)
    {
        i=M_x.size()-2;
    }
    if (j<0)
    {
        j=0;
    }
    if (j>=M_y.size()-1)
    {
        j=M_y.size()-2;
    }
    
    return M_a[i][j][0] + M_a[i][j][1]*x + M_a[i][j][2]*y + M_a[i][j][3]*x*y;
}
