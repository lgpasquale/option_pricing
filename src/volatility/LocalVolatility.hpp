#ifndef _LOCALVOLATILITY_HPP
#define _LOCALVOLATILITY_HPP
#include <lifev/core/LifeV.hpp>
#include <iostream>
#include <fstream>
#include <cmath>

#include "BlackScholes.hpp"
#include "MarketData.hpp"
#include "ImpliedVolatility.hpp"
#include "ThinPlateSpline.hpp"
#include "Spline.hpp"
#include "BilinearInterpolation.hpp"
#include "../filter/exportToGnuplot.hpp"

class LocalVolatility
{
public:
    LocalVolatility (boost::shared_ptr<MarketData> marketData, const double numInterpolatedMaturities, const double numInterpolatedStrikes);
    
    void exportCalls(const char* filename);
    void exportIV(const char* filename);
    void exportMarketLV(const char* filename);
    void exportLV(const char* filename);

    boost::shared_ptr<MarketData> getMarketData() {return M_marketData;}
    double getMaturityStepLenght() {return M_maturityStepLenght;}
    double getStrikeStepLenght() {return M_strikeStepLenght;}
    unsigned int getNumMaturities() {return M_numMaturities;}
    unsigned int getNumStrikes() {return M_numStrikes;}
    std::vector<double>& getMaturities() {return M_maturities;}
    std::vector<double>& getStrikes() {return M_strikes;}
    
    double getLocalVolatility (int strikeIndex, int maturityIndex);
    double getLocalVolatility (double assetPrice, double t);
    double getLocalVolatilityFirstDerivative (double assetPrice, double t);
    double getLocalVolatilitySecondDerivative (double assetPrice, double t);
    double getLocalVolatilityIntegral (double assetPrice1, double assetPrice2, double t);
    
protected:
    void filterMonotonePoints(const std::vector<double>& y, std::vector<bool>& selectedPoints, bool increasing=true);
    void filterConvexPoints(const std::vector<double>& x, const std::vector<double>& y, std::vector<bool>& selectedPoints);
    
    void updateCurrentSpline(const double& t);

    boost::shared_ptr<MarketData> M_marketData;
    double M_maturityStepLenght;
    double M_strikeStepLenght;
    int M_numMaturities;
    int M_numStrikes;
    std::vector<double> M_maturities;
    std::vector<double> M_strikes;
    
    std::vector<std::vector<double> > M_calls;
    std::vector<std::vector<double> > M_IV;
    std::vector<std::vector<double> > M_marketLV;
    std::vector<std::vector<double> > M_LV;
    boost::shared_ptr<ThinPlateSpline<2,1> > M_spline;
    
    std::vector<Spline> M_splines;
    Spline M_currentSpline;
    double M_currentTime;
    boost::shared_ptr<BilinearInterpolation> M_bilinearInterpolation;
};

#endif
