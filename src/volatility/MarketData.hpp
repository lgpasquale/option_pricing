#ifndef _MARKETDATA_HPP
#define _MARKETDATA_HPP
#include <lifev/core/LifeV.hpp>
#include <iostream>
#include <fstream>
#include <cmath>

#include "ImpliedVolatility.hpp"
#include "../filter/exportToGnuplot.hpp"

class MarketData
{
public:
    MarketData (const char* filename);
    
    void computeMarketIV();
    void exportMarketIV(const char* filename);

    double getAssetPrice() {return M_assetPrice;}
    double getRiskFreeRate() {return M_r;}
    double getDividendYield() {return M_q;}
    int getNumMaturities() {return M_numMarketMaturities;}
    int getNumStrikes() {return M_numMarketStrikes;}
    std::vector<double>& getMaturities() {return M_marketMaturities;}
    std::vector<double>& getStrikes() {return M_marketStrikes;}
    std::vector<std::vector<double> >& getMarketCalls() {return M_marketCalls;}
    double getMaturity(const int& i) {return M_marketMaturities[i];}
    double getStrike(const int& i) {return M_marketStrikes[i];}
    double getMarketCall(const int& maturityIndex, const int& strikeIndex) {return M_marketCalls[maturityIndex][strikeIndex];}
    double getMarketIV(const int& maturityIndex, const int& strikeIndex) {return M_marketIV[maturityIndex][strikeIndex];}
    
protected:
    double M_assetPrice;
    double M_r;
    double M_q;
    int M_numMarketMaturities;
    int M_numMarketStrikes;
    std::vector<double> M_marketMaturities;
    std::vector<double> M_marketStrikes;
    
    std::vector<std::vector<double> > M_marketCalls;
    std::vector<std::vector<double> > M_marketIV;
};

#endif
