#include "MarketData.hpp"

MarketData::MarketData (const char* filename)
{
    std::ifstream optionChainData;
    optionChainData.open (filename, std::ifstream::in);
    
    // Count the number of strike prices by counting the number of lines in the data file
    int lines = 0;
    std::string line;

    while(getline(optionChainData, line))
        ++lines;

    M_numMarketStrikes=lines-4;
    
    optionChainData.clear();
    optionChainData.seekg(0);
    
    // The first three numbers read are the asset price, the risk free rate and the dividend yield
    optionChainData >> M_assetPrice;
    optionChainData >> M_r;
    optionChainData >> M_q;
    
    // The first number read is the number of maturities available on the market
    optionChainData >> M_numMarketMaturities;

    // Resize vectors according to number of strikes and maturities
    M_marketMaturities.resize(M_numMarketMaturities);
    M_marketStrikes.resize(M_numMarketStrikes);
    M_marketCalls.resize(M_numMarketMaturities);
    for (int i=0; i<M_numMarketMaturities; ++i)
    {
        M_marketCalls[i].resize(M_numMarketStrikes);
    }

    // The rest of first row of the file contains the maturities
    for (int i=0; i<M_numMarketMaturities; ++i)
    {
        optionChainData >> M_marketMaturities[i];
        //M_marketMaturities[i]/=365;
    }
    
    for (int j=0; j<M_numMarketStrikes; ++j)
    {
        // The first number for each line is the strike price
        optionChainData >> M_marketStrikes[j];
        std::cout << M_marketStrikes[j] << "\t|\t" << std::flush;
        
        // Each line of the data file contains the call prices at different maturities
        for (int i=0; i<M_numMarketMaturities; ++i)
        {
            optionChainData >> M_marketCalls[i][j];
            std::cout << M_marketCalls[i][j] << "\t" << std::flush;
        }
        std::cout << std::endl;
    }

    optionChainData.close();
    
}

void MarketData::computeMarketIV()
{
    ImpliedVolatility impliedVolatility(M_assetPrice,M_r,M_q);

    // Resize vectors according to number of strikes and maturities
    M_marketIV.resize(M_numMarketMaturities);
    for (int i=0; i<M_numMarketMaturities; ++i)
    {
        M_marketIV[i].resize(M_numMarketStrikes);
    }
    
    for (int j=0; j<M_numMarketStrikes; ++j)
    {
        for (int i=0; i<M_numMarketMaturities; ++i)
        {
            M_marketIV[i][j]=impliedVolatility.compute(M_marketMaturities[i],M_marketStrikes[j],M_marketCalls[i][j]);
        }
    }

}

void MarketData::exportMarketIV(const char* filename)
{
    exportToGnuplot3d(filename,M_marketMaturities,M_marketStrikes,M_marketIV);
}
