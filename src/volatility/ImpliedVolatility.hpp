#ifndef _IMPLIEDVOLATILITY_HPP
#define _IMPLIEDVOLATILITY_HPP
#include <lifev/core/LifeV.hpp>
#include <iostream>
#include <fstream>
#include <cmath>

#include <boost/math/distributions.hpp>

#include "newtonsMethod.hpp"

class BSMarketCallDifference
{
public:
    BSMarketCallDifference(const double& assetPrice, const double& r, const double& q):
        M_assetPrice(assetPrice), M_r(r), M_q(q), M_normalDist(0,1) {};
        
    void setStrike(const double& strike) {M_strike = strike;}
    void setMaturity(const double& maturity) {M_T = maturity;}
    void setCallMarketPrice(const double& callMarketPrice) {M_callMarketPrice = callMarketPrice;}

        
    double operator()(const double& sigma);    
    
protected:
    double M_strike;
    double M_assetPrice;
    double M_r;
    double M_q;
    double M_T;
    double M_callMarketPrice;
    boost::math::normal_distribution<> M_normalDist;
};

class BSVega
{
public:
    BSVega(const double& assetPrice, const double& r, const double& q):
        M_assetPrice(assetPrice), M_r(r), M_q(q) {};
        
    void setStrike(const double& strike) {M_strike = strike;}
    void setMaturity(const double& maturity) {M_T = maturity;}
        
    double operator()(const double& sigma);    
    
protected:
    double M_strike;
    double M_assetPrice;
    double M_r;
    double M_q;
    double M_T;
};

class ImpliedVolatility
{
public:
    ImpliedVolatility (const double& assetPrice, const double& r, const double& q, const double& tolerance=1.e-8);
    
    double compute(const double& maturity, const double& strike, const double& call, const double& initialGuess=0.5);

protected:
    double M_assetPrice;
    double M_r;
    double M_q;
    
    BSMarketCallDifference M_bsMarketCallDifference;
    BSVega M_bsVega;
    
    double M_tolerance;
};

#endif
