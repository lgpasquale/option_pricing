#include "BlackScholes.hpp"

namespace BlackScholes
{

const boost::math::normal_distribution<> normalDist(0,1);

double bsEuropeanCall(const double& assetPrice, const double& strike, const double& T, const double& r, const double& q, const double& sigma)
{
    double d1 = (std::log(assetPrice/strike)+(r-q+0.5*sigma*sigma)*T)/(sigma*std::sqrt(T));
    double d2 = (std::log(assetPrice/strike)+(r-q-0.5*sigma*sigma)*T)/(sigma*std::sqrt(T));
    return assetPrice*std::exp(-q*T)*cdf(normalDist, d1) - strike*std::exp(-r*T)*cdf(normalDist, d2);
}

}
