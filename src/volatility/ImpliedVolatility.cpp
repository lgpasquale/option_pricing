#include "ImpliedVolatility.hpp"


double BSMarketCallDifference::operator()(const double& sigma)
{
    double d1 = (std::log(M_assetPrice/M_strike)+(M_r-M_q+0.5*sigma*sigma)*M_T)/(sigma*std::sqrt(M_T));
    double d2 = (std::log(M_assetPrice/M_strike)+(M_r-M_q-0.5*sigma*sigma)*M_T)/(sigma*std::sqrt(M_T));
    double bsCall = M_assetPrice*std::exp(-M_q*M_T)*cdf(M_normalDist, d1) - M_strike*std::exp(-M_r*M_T)*cdf(M_normalDist, d2);
    
    return bsCall-M_callMarketPrice;
}

double BSVega::operator()(const double& sigma)
{
    double d1 = (std::log(M_assetPrice/M_strike)+(M_r-M_q+0.5*sigma*sigma)*M_T)/(sigma*std::sqrt(M_T));
    double bsVega = M_assetPrice*std::exp(-M_q*M_T)*std::sqrt(M_T)*std::exp(-0.5*d1*d1)/std::sqrt(2.0*M_PI);
    
    return bsVega;
}

ImpliedVolatility::ImpliedVolatility (const double& assetPrice, const double& r, const double& q, const double& tolerance):
    M_assetPrice(assetPrice), M_r(r), M_q(q),
    M_bsMarketCallDifference(assetPrice, r, q), M_bsVega(assetPrice, r, q),
    M_tolerance(tolerance)
{

}

double ImpliedVolatility::compute(const double& maturity, const double& strike, const double& call, const double& initialGuess)
{
    double iv;
    
    // Update the functors used to compute IV
    M_bsMarketCallDifference.setStrike(strike);
    M_bsMarketCallDifference.setMaturity(maturity);
    M_bsMarketCallDifference.setCallMarketPrice(call);
    M_bsVega.setStrike(strike);
    M_bsVega.setMaturity(maturity);
    // Find the IV using Newton's method
    newtonsMethod(M_bsMarketCallDifference, M_bsVega, iv,initialGuess,M_tolerance);

    return iv;
}
