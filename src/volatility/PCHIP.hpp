#ifndef _PCHIP_HPP
#define _PCHIP_HPP

#include <algorithm>
#include <iostream>
#include <vector>

class PCHIP
{
public:
    PCHIP(const std::vector<double>& x, const std::vector<double>& y);

    double evaluate(const double& x);
    
protected:
    std::vector<double> M_x;
    
    // In each interval i the interpolant will be computed as:
    // a[i][0] + x*(a[i][1] + x*(a[i][2] + x*a[i][3])
    std::vector<std::vector<double> > M_a;
};

#endif
