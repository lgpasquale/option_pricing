#include "PCHIP.hpp"

PCHIP::PCHIP(const std::vector<double>& x, const std::vector<double>& y):
    M_x(x), M_a(x.size()-1, std::vector<double>(4) )
{
    int n=x.size();
    if (n<2)
    {
        std::cout << "Must have at least two values to interpolate" << std::endl;
    }
    
    std::vector<double> m(n);
    
    // Except for the first and last points,
     //m[i] is the average between the increments in (x[i-1],x[i]) and (x[i],x[i+1])
    //m[0]=(y[1]-y[0])/(x[1]-x[0]);
    m[0]=0.8*(1.25*(y[1]-y[0])/(x[1]-x[0])-0.25*(y[2]-y[1])/(x[2]-x[1]));
    //m[0]=-(2.*(x[1]-x[0])+(x[2]-x[1]))/(x[1]-x[0])/((x[1]-x[0])+(x[2]-x[1]))*y[0]
        //+((x[1]-x[0])+(x[2]-x[1]))/(x[1]-x[0])/(x[2]-x[1])*y[1]
        //-(x[1]-x[0])/(x[2]-x[1])/((x[1]-x[0])+(x[2]-x[1]))*y[2];
    for (int i=1; i<n-1; ++i)
    {
        m[i]=0.5*( (y[i]-y[i-1])/(x[i]-x[i-1]) + (y[i+1]-y[i])/(x[i+1]-x[i]) );
    }
    //m[n-1]=(y[n-1]-y[n-2])/(x[n-1]-x[n-2]);
    m[n-1]=1.25*(y[n-1]-y[n-2])/(x[n-1]-x[n-2])-0.25*(y[n-2]-y[n-3])/(x[n-2]-x[n-3]);
    //m[0]=(2.*(x[n-1]-x[n-2])+(x[n-2]-x[n-3]))/(x[n-1]-x[n-2])/((x[n-1]-x[n-2])+(x[n-2]-x[n-3]))*y[n-1]
        //-((x[n-1]-x[n-2])+(x[n-2]-x[n-3]))/(x[n-1]-x[n-2])/(x[n-2]-x[n-3])*y[n-2]
        //+(x[n-1]-x[n-2])/(x[n-2]-x[n-3])/((x[n-1]-x[n-2])+(x[n-2]-x[n-3]))*y[n-3];

    // If two points have the same value the interpolant must be flat between them
    for (int i=0; i<n-1; ++i)
    {
        if ( y[i+1]==y[i] )
        {
            m[i]=0.;
            m[i+1]=0.;
        }std::cout<<m[i]<<" "<<std::flush;
    }std::cout<<std::endl;

    // Compute the coefficients of the cubic polynomials
    for (int i=0; i<n-1; ++i)
    {
        M_a[i][0]=y[i];
        M_a[i][1]=m[i]*(M_x[i+1]-M_x[i]);
        M_a[i][2]=-3.*y[i]-2.*m[i]*(M_x[i+1]-M_x[i])+3.*y[i+1]-m[i+1]*(M_x[i+1]-M_x[i]);
        M_a[i][3]=2.*y[i]+m[i]*(M_x[i+1]-M_x[i])-2.*y[i+1]+m[i+1]*(M_x[i+1]-M_x[i]);
    }
}

double PCHIP::evaluate(const double& x)
{
    std::vector<double>::iterator up;
    up = std::upper_bound (M_x.begin(), M_x.end(), x);
    int i=std::distance(M_x.begin(),up)-1;

    if (i<0)
    {
        i=0;
    }
    if (i>=M_x.size()-1)
    {
        i=M_x.size()-2;
    }
    
    double t=(x-M_x[i])/(M_x[i+1]-M_x[i]);
    return M_a[i][0] + t*(M_a[i][1] + t*(M_a[i][2] + t*M_a[i][3]) );
}
