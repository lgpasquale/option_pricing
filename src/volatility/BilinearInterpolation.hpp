#ifndef _BILINEARINETRPOLATION_HPP
#define _BILINEARINTERPOLATION_HPP

#include <algorithm>
#include <iostream>
#include <vector>

class BilinearInterpolation
{
public:
    BilinearInterpolation(const std::vector<double>& x, const std::vector<double>& y, const std::vector<std::vector<double> >& z);

    double evaluate(const double& x, const double& y);
    
protected:
    std::vector<double> M_x;
    std::vector<double> M_y;
    //std::vector<std::vector<double> > M_z
    
    // In each interval i,j the interpolant will be computed as:
    // a[i][j][0] + x*a[i][j][1] + y*a[i][j][2] + x*y*a[i][j][3]
    std::vector<std::vector<std::vector<double> > > M_a;
};

#endif
