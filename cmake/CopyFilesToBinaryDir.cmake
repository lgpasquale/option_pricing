FUNCTION(COPY_FILES_TO_BINARY_DIR TARGET_NAME)
    set(FEST_FILES_LIST)
    foreach(f ${ARGN})
        # do something interesting with 'f' -> other optional arg list
        add_custom_command(
        #    TARGET op PRE_BUILD
            OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${f}
            COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/${f} ${CMAKE_CURRENT_BINARY_DIR}/${f}
            DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/${f}
            )
        SET(DEST_FILE_FULL "${CMAKE_CURRENT_BINARY_DIR}/${f}")
        LIST(APPEND DEST_FILES_LIST "${DEST_FILE_FULL}")
    endforeach()
    ADD_CUSTOM_TARGET(${TARGET_NAME} ALL DEPENDS ${DEST_FILES_LIST}) 

ENDFUNCTION()
