#!/bin/bash
# 
# CMAKE_BUILD_TYPE: "DEBUG" or "RELEASE".
#
# CMAKE_VERBOSE_MAKEFILE: Set to ON if you prefer verbose Make output.
#
OP_SOURCE_DIR=${HOME}/polimi/finanza2/option_pricing/op
OP_BUILD_DIR=${HOME}/polimi/finanza2/option_pricing/op-build-dbg

mkdir -p ${OP_BUILD_DIR}
cd ${OP_BUILD_DIR}

cmake \
  -D LIFEV_PATH:FILEPATH="${HOME}/polimi/lifev/lifev-install-opt" \
  -D CMAKE_CXX_FLAGS_DEBUG:STRING="-O0 -pg -g3 -Wno-unused-local-typedefs" \
  -D CMAKE_CXX_FLAGS_RELEASE:STRING="-O3 -mtune=native -march=native -msse -msse2 -Wno-unused-local-typedefs" \
  -D CMAKE_BUILD_TYPE:STRING=DEBUG \
  -D CMAKE_VERBOSE_MAKEFILE:BOOL=OFF \
  -DCMAKE_INSTALL_PREFIX:PATH="${HOME}/polimi/finanza2/option_pricing/op-install" \
  ${OP_SOURCE_DIR}


